﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;


namespace LoanApp.Controllers
{
    public class HomeController : Controller
    {

        LoanAppDBEntities db = new LoanAppDBEntities();
        WebAdo.HomePage home = new WebAdo.HomePage();

        // GET: Home
        public ActionResult Index()

        {


            if (_app.ui.current.User.Employee.BranchID != 1) { return RedirectToAction("BranchHome"); }



            loanCommonData();

            

            var query = (from p in db.LoansRequests
                        group p by DbFunctions.TruncateTime(p.RequestDate) into g
                        select new WebAdo.chart
                        {
                             ldate =g.Key.Value,
                            count = (int)g.Count()
                           
                        }).ToList();



            home.NewContrack = LoadLineChart(query);


            List<LoansRequest> dl = new List<LoansRequest>();

            if (home.Loans.All > 0)
            {


                for (int i = 0; i < db.LoanStatuses.Max(x => x.StatusID); i++)
                {
                    home.Donut.Add(new WebAdo.DonutProperty { label = db.LoanStatuses.FirstOrDefault(x => x.StatusID == i).StatusName });
                    home.Donut[i].value = db.LoansRequests.Where(x => x.StatusID == i).Count();
                    home.Donut[i].value = (home.Donut[i].value / home.Loans.All);
                    home.Donut[i].value = home.Donut[i].value * 100;
                    home.Donut[i].value = Math.Round(home.Donut[i].value, 1);
                }

            }

           
            var s1 = db.LoansRequests.Where(x => x.StatusID == 1).ToList();
            home.DelayedLoans1 = detectDelayed(s1, 6);


            var s4 = db.LoansRequests.Where(x => x.StatusID == 4).ToList();
            // dl.AddRange(detectDelayed(s4, 48));
            home.DelayedLoans4 = detectDelayed(s4, 48);

            var s5 = db.LoansRequests.Where(x => x.StatusID == 5).ToList();
            // dl.AddRange(detectDelayed(s5, 4));
            home.DelayedLoans5 = detectDelayed(s5, 4);

           // home.DelayedLoans = dl;

            LoanApp.Models.Classes.Send.TextMessage sms = new Models.Classes.Send.TextMessage();
            var s = sms.GetBalance();

            if (s != "error") { var sl = s.Split('/'); home.SmsBalance = sl[1]; }
            else { home.SmsBalance = s; }
           





            return View(home);
        }
        public ActionResult BranchHome()
        {

          //  loanCommonData();

            var al = db.Audits.Where(x => x.NewStatusID == 4 && x.LoansRequest.BranchID  == _app.ui.current.User.Employee.BranchID);

            var query = (from p in al
                         group p by DbFunctions.TruncateTime(p.AuditDate) into g
                         select new WebAdo.chart
                         {
                             ldate = g.Key.Value,
                             count = (int)g.Count()

                         }).ToList();




            home.NewContrack = LoadLineChart(query);




            home.DelayedLoans4 = detectDelayed(db.LoansRequests.Where(x=> x.StatusID == 4 && x.BranchID == _app.ui.current.User.Employee.BranchID).ToList()
                , 48);


            return View(home);
        }
        public ActionResult Search( string keyword )
        {
            WebAdo.SearchPage search = new WebAdo.SearchPage();
            search.Keyword = keyword;


            if (string.IsNullOrEmpty(keyword)) { return View(search); }

            var splstr = keyword.Trim().Split(' ');



            foreach (var s in splstr)
            {

                if (s.Any(x => Char.IsLetter(x)))
                {
                    search.Users.AddRange(db.UsersInfoes.Where(x => x.Name_f.Contains(s) || x.Name_l.Contains(s) || 
                    x.Name_m.Contains(s) && !search.Users.Any(c => x.UserID == c.UserID)).ToList());
                    search.Loans.AddRange(db.LoansRequests.Where((x => x.Customer.Job.JobName.Contains(s) ||
                    x.Customer.City.CityName.Contains(s) || x.Commitments.Any(c => c.CommitmentName.Contains(s)) && !search.Loans.Any(y => y.LoanID == x.LoanID))).ToList());
                }

                else

                {
                    search.Users.AddRange(db.UsersInfoes.Where(x => x.PhoneNumber.Contains(s) && !search.Users.Any(c => x.UserID == c.UserID)).ToList());
                    search.Loans.AddRange(db.LoansRequests.Where(x => x.LoanID.ToString().Contains(s) ||
                     x.Commitments.Any(c => c.CommitmentValue.ToString().Contains(s)) && !search.Loans.Any(y => y.LoanID == x.LoanID)).ToList());
                }

                return View(search);
            }


            return View(search);
        }
        // client wellcome screen
       



        private void loanCommonData()
        {
            home.People.CustomerCount = db.Customers.Count();
            home.People.SalesRepCount = db.SalesRepresentatives.Count();
            home.People.BankRepCount = db.BankRepresentatives.Count();
            home.People.EmpCount = db.Employees.Count();


            // loans

            home.Loans.All = db.LoansRequests.Count();
            home.Loans.Declined = db.LoansRequests.ToList().Where(x => _app.LoanReq.DeclinedStatus().Contains(x.StatusID)).Count();
            home.Loans.Open = db.LoansRequests.ToList().Where(x => !_app.LoanReq.ClosedStatus().Contains(x.StatusID)).Count();
            home.Loans.Complted = db.LoansRequests.ToList().Where(x => x.StatusID == 9).Count();

            var o = db.Audits.Where(x=> x.NewStatusID != null).ToList().OrderByDescending(x => x.AuditDate).ToList().Where(x =>
            !_app.LoanReq.ClosedStatus().Contains(x.LoansRequest.StatusID)).FirstOrDefault();
            home.Loans.OpenTill = tillDate(o); //o.AuditDate;

            var c = db.Audits.Where(x => x.NewStatusID != null).ToList().OrderByDescending(x => x.AuditDate).ToList().Where(x =>
            _app.LoanReq.ClosedStatus().Contains(x.LoansRequest.StatusID)).FirstOrDefault();
            home.Loans.CompltedTill = tillDate(c);//c.AuditDate;

            var dec = db.Audits.Where(x => x.NewStatusID != null).ToList().OrderByDescending(x => x.AuditDate).ToList().Where(x =>
           _app.LoanReq.DeclinedStatus().Contains(x.NewStatusID.Value)).FirstOrDefault();
            home.Loans.DeclinedTill = tillDate(dec); //dec.AuditDate;

        }
        private DateTime tillDate(Audit o)
        {
            if (o == null) { return DateTime.Today.AddYears(-100); }
            else  {return o.AuditDate; }

        }
        private string LoadLineChart(List<WebAdo.chart> query)
        {
            var str = "["; 

            query = query.OrderBy(x => x.ldate).ToList();


            foreach (var item in query)
            {

                if (item.ldate >= _app.ui.DateNow.AddDays(-30))

                {
                    str += "[" +
                         // item.ldate.Year.ToString() +
                         //  item.ldate.Month.ToString() +
                         item.ldate.DayOfYear.ToString() + "," + item.count.ToString() + "],";
                }

            }
            str = str.Remove(str.Length - 1);
            str += "]";
            return HttpUtility.HtmlEncode(str);


        }
        private List<LoansRequest> detectDelayed(List<LoansRequest> ls, int hours)
        {

            List<LoansRequest> dl = new List<LoansRequest>();
            foreach (var item in ls)
            {
                var a = item.Audits.OrderByDescending(x => x.AuditDate).FirstOrDefault(x => x.NewStatusID == item.StatusID);
                if (a != null) { if (a.AuditDate.AddHours(hours) < _app.ui.DateNow) { item.lastAudit = a; dl.Add(item); } }
               




            }

            return dl;
        }

    }
}