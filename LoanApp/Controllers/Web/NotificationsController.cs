﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;
using LoanApp.Models.Classes;

namespace LoanApp.Controllers.Web
{
    public class NotificationsController : Controller
    {
        private LoanAppDBEntities db = new LoanAppDBEntities();

        // GET: Notifications
        public ActionResult Index()
        {
            var notifications = db.Notifications.Include(n => n.Employee).OrderByDescending(x=> x.SentDate).Where(x=> x.ToUser == null);
            return View(notifications.ToList());
        }


        //[AllowAnonymous]
        //public ActionResult mailTest()
        //{
        //    Send.eMail e = new Send.eMail();
        //        e.SendMail();
        //    return View();
        //}


        //// GET: Notifications/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Notification notification = db.Notifications.Find(id);
        //    if (notification == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(notification);
        //}

        // GET: Notifications/Create
        public ActionResult Create()
        {
            //  ViewBag.CreatedEmp = new SelectList(db.Employees, "EmpID", "EmpID");
            Notification n = new Notification();
            return View(n);
        }

        // POST: Notifications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Notification noti)
        {
            if (ModelState.IsValid)
            {

                Notification n = new Notification();
                Send.Notifications not = new Send.Notifications();
                noti.Photo = _app.ui.photo.replacePhoto(n.Photo, Request.Files);
                noti.CreatedEmp = _app.ui.current.User.UserID;
                noti.SentDate = _app.ui.DateNow;
                noti.PageName = _app.Notifecations.NotifecationsPage();
                db.Notifications.Add(noti);
                db.SaveChanges();

              //  var ms = db.MobileSessions.Select(x => x.MobileID).Distinct();

                if (noti.ToCustomers)
                {
                    var cl = db.Customers.ToList();
                    foreach (var c in cl)
                    {
                        not.SendNotification(c.UsersInfo, noti );
                    }

                }

                if (noti.ToBankReps)
                {
                    var brl = db.BankRepresentatives.ToList();
                    foreach (var c in brl)
                    {
                        not.SendNotification(c.UsersInfo, noti);
                    }

                }

                if (noti.ToSalesReps)
                {
                    var srl = db.SalesRepresentatives.ToList();
                    foreach (var c in srl)
                    {
                        not.SendNotification(c.UsersInfo, noti);
                    }

                }


                _app.ui.msg.Show = "تم ارسال التنبيه بنجاح";
                return RedirectToAction("Index");
            }

          
            //  ViewBag.CreatedEmp = new SelectList(db.Employees, "EmpID", "EmpID", notification.CreatedEmp);
            return View(noti);
        }

        // GET: Notifications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notification notification = db.Notifications.Find(id);
            if (notification == null)
            {
                return HttpNotFound();
            }
            ViewBag.CreatedEmp = new SelectList(db.Employees, "EmpID", "EmpID", notification.CreatedEmp);
            return View(notification);
        }

        // POST: Notifications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NoID,Header,Continet,CreatedEmp,Photo,SentDate")] Notification notification)
        {
            if (ModelState.IsValid)
            {
                db.Entry(notification).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CreatedEmp = new SelectList(db.Employees, "EmpID", "EmpID", notification.CreatedEmp);
            return View(notification);
        }

        // GET: Notifications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notification notification = db.Notifications.Find(id);
            if (notification == null)
            {
                return HttpNotFound();
            }
            return View(notification);
        }

        // POST: Notifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Notification notification = db.Notifications.Find(id);
            db.Notifications.Remove(notification);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
