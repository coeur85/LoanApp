﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;
using System.Data.Entity;

namespace LoanApp.Controllers.Web
{
    public class CustomersController : Controller
    {
        // GET: Customers

        LoanAppDBEntities db = new LoanAppDBEntities();
        private List<Customer> cl() { return db.Customers.OrderByDescending(x => x.CustomerID).ToList(); }


        public ActionResult Index()
        {
            
            return View(cl());
        }

        // GET: Customers/Details/5
        public ActionResult Details(int id)
        {
            var c = cl().FirstOrDefault(x => x.CustomerID == id);
            if (c == null) { return HttpNotFound(); }
            ViewBag.formMode = "details";
            return View(c);
        }

        //// GET: Customers/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Customers/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Customers/Edit/5
        public ActionResult Edit(int id)
        {
            var c = cl().FirstOrDefault(x => x.CustomerID == id);
            if (c == null) { return HttpNotFound(); }
            ViewBag.CityID = new SelectList(db.Cities, "CityID", "CityName", c.CityID);
            ViewBag.JobID = new SelectList(db.Jobs, "JobID", "JobName", c.JobID);
            ViewBag.formMode = "edit";
            return View(c);
        }

        // POST: Customers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Customer cu)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var oldcust = db.Customers.FirstOrDefault(x => x.CustomerID == cu.UsersInfo.UserID);

                    oldcust.NationalID = cu.NationalID;
                    oldcust.JobID = cu.JobID;
                    oldcust.CityID = cu.CityID;

                    oldcust.UsersInfo.ActiveAccount = cu.UsersInfo.ActiveAccount;
                    oldcust.UsersInfo.Name_f = cu.UsersInfo.Name_f;
                    oldcust.UsersInfo.Name_l = cu.UsersInfo.Name_l;
                    oldcust.UsersInfo.Name_m = cu.UsersInfo.Name_m;
                    oldcust.UsersInfo.Photo = _app.ui.photo.replacePhoto(oldcust.UsersInfo.Photo, Request.Files);
                    oldcust.UsersInfo.PhoneNumber = cu.UsersInfo.PhoneNumber;

                    cu.UsersInfo.Photo = oldcust.UsersInfo.Photo;

                    db.Entry(oldcust).State = EntityState.Modified;
                    db.SaveChanges();
                    _app.ui.msg.Show = "تم الحفظ بنجاح";

                    var lr = oldcust.LoansRequests.Where(x => x.StatusID == 3).ToList();

                    if (lr.Count > 0)
                    {
                        foreach (var item in lr)
                        {
                            _app.LoanReq.LoanEquation(item);
                        }

                        db.SaveChanges();
                    }


                    return RedirectToAction("Index");

                }
                

                // TODO: Add update logic here

                //var c = cl().FirstOrDefault(x => x.CustomerID == id);
                //if (c == null) { return HttpNotFound(); }
                //c.UsersInfo.ActiveAccount = !c.UsersInfo.ActiveAccount;
              
            }
            catch
            {
               
            }

            ViewBag.CityID = new SelectList(db.Cities, "CityID", "CityName", cu.CityID);
            ViewBag.JobID = new SelectList(db.Jobs, "JobID", "JobName", cu.JobID);
            ViewBag.formMode = "edit";
            return View(cu);


        }


        public ActionResult RestPassword(int id)

        {
            var cu = db.Customers.Find(id);
            //em.UsersInfo.Password = "12345678";
            //db.SaveChanges();
            _app.model ui = new _app.model();
            ui.restPassword(id, "123456789");

            LoanApp.Models.Classes.Send.TextMessage text = new Models.Classes.Send.TextMessage();
            text.SendRestPasswordTest(cu.UsersInfo.PhoneNumber, cu.UsersInfo.Password);

            _app.ui.msg.Show = "تم اعاده كلمه المرور الي " + cu.UsersInfo.Password + " وارسالها الي العميل في رساله نصيه ";

            return RedirectToAction("Edit", new { id = cu.UsersInfo.UserID });
            // return View();
        }

        //// GET: Customers/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: Customers/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
