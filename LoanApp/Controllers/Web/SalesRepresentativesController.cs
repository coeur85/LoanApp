﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Controllers
{
    public class SalesRepresentativesController : Controller
    {
        private LoanAppDBEntities db = new LoanAppDBEntities();

        // GET: SalesRepresentatives
        public ActionResult Index()
        {
            var salesRepresentatives = db.SalesRepresentatives.Include(s => s.UsersInfo);
            return View(salesRepresentatives.ToList());
        }

        // GET: SalesRepresentatives/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesRepresentative salesRepresentative = db.SalesRepresentatives.Find(id);
            if (salesRepresentative == null)
            {
                return HttpNotFound();
            }
            ViewBag.formMode = "details";
            return View(salesRepresentative);
        }

        // GET: SalesRepresentatives/Create
        public ActionResult Create()
        {
            // ViewBag.SalesRepID = new SelectList(db.UsersInfoes, "UserID", "Name_f");
            SalesRepresentative sr = new SalesRepresentative();
            sr.UsersInfo = new UsersInfo(true);
            ViewBag.formMode = "create";
            return View(sr);
        }

        // POST: SalesRepresentatives/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SalesRepresentative salesRepresentative)
        {

            try
            {
                if (ModelState.IsValid)
                {
                UsersInfo u = new UsersInfo(true);
                salesRepresentative.UsersInfo.Photo = _app.ui.photo.replacePhoto(u.Photo, Request.Files);
                salesRepresentative.UsersInfo.ActiveAccount = true;
              //  salesRepresentative.UsersInfo.RoleID = 3;
                db.SalesRepresentatives.Add(salesRepresentative);
                db.SaveChanges();
                _app.ui.msg.Show = "تمت الاضافه بنجاح";
                return RedirectToAction("Index");
                 }
            }
            catch (Exception)
            {

                
            }



            //   ViewBag.SalesRepID = new SelectList(db.UsersInfoes, "UserID", "Name_f", salesRepresentative.SalesRepID);
            ViewBag.formMode = "create";
            return View(salesRepresentative);
        }

        // GET: SalesRepresentatives/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesRepresentative salesRepresentative = db.SalesRepresentatives.Find(id);
            if (salesRepresentative == null)
            {
                return HttpNotFound();
            }
            //  ViewBag.SalesRepID = new SelectList(db.UsersInfoes, "UserID", "Name_f", salesRepresentative.SalesRepID);
            ViewBag.formMode = "edit";
            return View(salesRepresentative);
        }

        // POST: SalesRepresentatives/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( SalesRepresentative salesRepresentative)
        {
            var oldrep = db.SalesRepresentatives.Find(salesRepresentative.UsersInfo.UserID);
            try
            {

                if (ModelState.IsValid)
            {
               

                oldrep.UsersInfo.Name_f = salesRepresentative.UsersInfo.Name_f;
                oldrep.UsersInfo.Name_l = salesRepresentative.UsersInfo.Name_l;
                oldrep.UsersInfo.Name_m = salesRepresentative.UsersInfo.Name_m;
                oldrep.UsersInfo.PhoneNumber = salesRepresentative.UsersInfo.PhoneNumber;
                oldrep.UsersInfo.Photo = _app.ui.photo.replacePhoto(oldrep.UsersInfo.Photo, Request.Files);
               
                oldrep.UsersInfo.ActiveAccount = salesRepresentative.UsersInfo.ActiveAccount;

                db.Entry(oldrep).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }



            }
            catch (Exception)
            {

               
            }

            ViewBag.formMode = "edit";
            return View(oldrep);
        }

        public ActionResult RestPassword(int id)

        {
            var br = db.SalesRepresentatives.Find(id);
            //em.UsersInfo.Password = "12345678";
            //db.SaveChanges();
            _app.model ui = new _app.model();
            ui.restPassword(id, "123456789");

            _app.ui.msg.Show = "تم اعاده كلمه المرور الي " + br.UsersInfo.Password;

            return RedirectToAction("Edit", new { id = br.UsersInfo.UserID });
            // return View();
        }

        // GET: SalesRepresentatives/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesRepresentative salesRepresentative = db.SalesRepresentatives.Find(id);
            if (salesRepresentative == null)
            {
                return HttpNotFound();
            }
            ViewBag.formMode = "details";
            return View(salesRepresentative);
        }

        // POST: SalesRepresentatives/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SalesRepresentative salesRepresentative = db.SalesRepresentatives.Find(id);
            db.SalesRepresentatives.Remove(salesRepresentative);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
