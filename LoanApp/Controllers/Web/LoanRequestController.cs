﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;
using System.Net;

namespace LoanApp.Controllers
{
    [WebPagesAuthorize]
    public class LoanRequestController : Controller
    {

        LoanAppDBEntities db = new LoanAppDBEntities();


        private IEnumerable<LoansRequest> Lrl
        { get
            {
               // var l = db.LoansRequests.OrderByDescending(x => x.RequestDate);
                var u = _app.ui.current.User;
                // if (u.Employee.BranchID == 1) { return l; }
                //  else { return l.Where(x => x.BranchID == u.Employee.BranchID); }
                return u.Employee.MyLoanRequest.OrderByDescending(x => x.RequestDate);
            }
        }

        private int LoansPerPage = 5;

        // GET: LoanRequest
        public ActionResult Index()
        {

            // var l = db.LoansRequests.ToList().OrderByDescending(x => x.RequestDate);
            return View(Lrl.Take(LoansPerPage).ToList());
        }

        // GET: LoanRequest/Details/5
        public ActionResult Details(int id)
        {
            var b = db.LoansRequests.Find(id);
            //  var c = b.Commitments.ToList()[0];

            //  ViewBag.RepID = new SelectList(db.BankRepresentatives, "UserID", "FullName");
            return View(b);
        }



        // GET: LoanRequest/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LoanRequest/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LoanRequest/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LoanRequest/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }




        public ActionResult Accepted()
        {
            return View(Lrl.Where(x => x.StatusID == 1).Take(LoansPerPage).ToList());
        }
        public ActionResult DeclinedByApp()
        {
            return View(Lrl.Where(x => x.StatusID == 2).Take(LoansPerPage).ToList());
        }
        public ActionResult inCompelete()
        {
            return View(Lrl.Where(x => x.StatusID == 3).Take(LoansPerPage).ToList());
        }
        public ActionResult Sent2Branch()
        {
            var u = _app.ui.current.User;

            if (u.Employee.BranchID == 1)
            { return View(Lrl.Where(x => x.StatusID == 4).Take(LoansPerPage).ToList()); }
            else
            {
                return View(Lrl.Where(x => x.BranchID == u.Employee.BranchID && x.StatusID == 4).Take(LoansPerPage).ToList());
            }

        }
        public ActionResult Sent2Bank()
        {
            return View(Lrl.Where(x => x.StatusID == 5).Take(LoansPerPage).ToList());
        }
        public ActionResult ReceivedFromBank()
        {
            return View(Lrl.Where(x => x.StatusID == 6).Take(LoansPerPage).ToList());
        }
        public ActionResult Compeleted()
        {
            return View(Lrl.Where(x => x.StatusID == 7).Take(LoansPerPage).ToList());
        }
        public ActionResult DeclinedByEmp()
        {
            return View(Lrl.Where(x => x.StatusID == 8).Take(LoansPerPage).ToList());
        }
        public ActionResult AccedptedAndFinished()
        {
            return View(Lrl.Where(x => x.StatusID == 9).Take(LoansPerPage).ToList());
        }
        public ActionResult DeclineLoanByBranch()
        {
            return View(Lrl.Where(x => x.StatusID == 10).Take(LoansPerPage).ToList());
        }
        public ActionResult UnConfirmedLoans()
        {

            return View(Lrl.Where(x => x.StatusID == 0).Take(LoansPerPage).ToList());
        }
        public ActionResult Archive()
        {
            return View(Lrl.Where(x => x.StatusID == 8 || x.StatusID ==9 || x.StatusID == 10 ).Take(LoansPerPage).ToList());
        }

       


        [HttpPost]
        public ActionResult ScroolHandler(WebAdo.LoansScrool ls)
        {

            if (ls.MaxLoanID == 0) { ls.MaxLoanID = db.LoansRequests.Max(x => x.LoanID) + 1; }

            List<LoansRequest> l = new List<LoansRequest>();


            if (ls.Status == 4 && _app.ui.current.User.Employee.BranchID != 1)
            { l = Lrl.Where(x => x.BranchID == _app.ui.current.User.Employee.BranchID && x.StatusID == ls.Status && x.LoanID < ls.MaxLoanID).Take(LoansPerPage).ToList(); }

            if (ls.Status == 8910) {

                l = Lrl.Where(x => x.StatusID == 8 || x.StatusID == 9 || x.StatusID == 10).Take(LoansPerPage).ToList();
                if (_app.ui.current.User.Employee.BranchID != 1) {
                    l = l.Where(x => x.BranchID == _app.ui.current.User.Employee.BranchID).ToList();
                }
            }

            else if (ls.Status > 0)
            { l = Lrl.Where(x => x.StatusID == ls.Status && x.LoanID < ls.MaxLoanID).Take(LoansPerPage).ToList(); }

            else
            { l = Lrl.Where(x => x.LoanID < ls.MaxLoanID).Take(LoansPerPage).ToList(); }
             


            if (l.Count > 0) { return PartialView("~/Views/Shared/_LoanRequestScroolLoader.cshtml", l); }
            else { return null; }
 
            
            





            //return View();
        }

    }
}
