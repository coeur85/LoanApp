﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Controllers
{
    public class EmployeController : Controller
    {
        private LoanAppDBEntities db = new LoanAppDBEntities();

        // GET: Employe
        public ActionResult Index()
        {
            var employees = db.Employees.Include(e => e.Branch).Include(e => e.UsersInfo);
            return View(employees.ToList());
        }

        // GET: Employe/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.formMode = "details";
            return View(employee);
        }

        // GET: Employe/Create
        public ActionResult Create()
        {
            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName");
            Employee emp = new Employee();
            emp.UsersInfo = new UsersInfo(true);
            ViewBag.formMode = "create";
            ViewBag.AccountType = "Emp";
            emp.BranchID = 1;
            return View(emp);
        }

        // POST: Employe/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Employee employee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UsersInfo u = new UsersInfo(true);
                    employee.UsersInfo.Photo = _app.ui.photo.replacePhoto(u.Photo, Request.Files);
                    employee.UsersInfo.ActiveAccount = true;

                    //if (employee.BranchID == 1) { employee.UsersInfo.RoleID = 1; }
                    //else { employee.UsersInfo.RoleID = 2; }
                    UpdateEmpPermissions(employee);
                    db.Employees.Add(employee);
                    db.SaveChanges();
                    _app.ui.msg.Show = "تمت الاضافه بنجاح";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {

                
            }


           

            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName", employee.BranchID);
            ViewBag.AccountType = "Emp";
            ViewBag.formMode = "create";
            return View(employee);
        }

        // GET: Employe/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName", employee.BranchID);
            //  ViewBag.EmpID = new SelectList(db.UsersInfoes, "UserID", "Name_f", employee.EmpID);
            ViewBag.formMode = "edit";
            return View(employee);
        }

        // POST: Employe/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Employee  emp)
        {

            var oldEmp = db.Employees.Find(emp.EmpID);
            try
            {
                     if (ModelState.IsValid)
                                {
                

               
               
                                   
                                    oldEmp.BranchID = emp.BranchID;
                                    oldEmp.UsersInfo.ActiveAccount = emp.UsersInfo.ActiveAccount;
                                    oldEmp.UsersInfo.Name_f = emp.UsersInfo.Name_f;
                                    oldEmp.UsersInfo.Name_l = emp.UsersInfo.Name_l;
                                    oldEmp.UsersInfo.Name_m = emp.UsersInfo.Name_m;
                                    oldEmp.UsersInfo.PhoneNumber = emp.UsersInfo.PhoneNumber;
                                    oldEmp.UsersInfo.Photo = _app.ui.photo.replacePhoto(oldEmp.UsersInfo.Photo, Request.Files);

                                   
                                    UpdateEmpPermissions(oldEmp);

                                  //  db.Entry(oldEmp).CurrentValues.SetValues(emp.UsersInfo);
                                   db.Entry(oldEmp).State = EntityState.Modified;
                                    if (db.SaveChanges() > 0) { _app.ui.msg.Show = "تم الحفظ بنجاح"; }
                                    return RedirectToAction("Index");
                     }
            }
            catch (Exception)
            {

               
            }

           
            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName", oldEmp.BranchID);      
            ViewBag.formMode = "edit";
            return View(oldEmp);
        }


        public ActionResult RestPassword(int id)

        {
            var em = db.Employees.Find(id);
            //em.UsersInfo.Password = "12345678";
            //db.SaveChanges();
            _app.model ui = new _app.model();
            ui.restPassword(id, "123456789");

            _app.ui.msg.Show = "تم اعاده كلمه المرور الي " + em.UsersInfo.Password;

          return  RedirectToAction("Edit", new { id = em.EmpID });
           // return View();
        }


        // GET: Employe/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.formMode = "details";
            return View(employee);
        }

        // POST: Employe/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            _app.ui.msg.Show = "تم الحذف بنجاح";
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        private void UpdateEmpPermissions(Employee emp)
        {

            var per = db.MenuHeaders.Where(x => x.OnlyActionControl == false).ToList();

            List<MenuHeader> empMenu = new List<MenuHeader>();


           
            foreach (var item in per)
            {


                
                if (Request.Form["mh_" + item.HeaderID.ToString()] == "on") { empMenu.Add(item); }


            }

            // all branch emps must have access to back reps

           



            emp.UsersInfo.MenuHeaders.Clear();
            emp.UsersInfo.MenuHeaders = empMenu;






        }
    }
}
