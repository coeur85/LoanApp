﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Controllers
{
    public class BankRepresentativesController : Controller
    {
        private LoanAppDBEntities db = new LoanAppDBEntities();

        // GET: BankRepresentatives
        public ActionResult Index()
        {
            var bankRepresentatives = db.BankRepresentatives.Include(b => b.Bank).Include(b => b.UsersInfo).Include(b=> b.Branch);

            if (_app.ui.current.User.Employee.BranchID != 1)
            { bankRepresentatives = bankRepresentatives.Where(x => x.BranchID == _app.ui.current.User.Employee.BranchID); }

            return View(bankRepresentatives.ToList());
        }

        // GET: BankRepresentatives/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankRepresentative bankRepresentative = db.BankRepresentatives.Find(id);
            if (bankRepresentative == null)
            {
                return HttpNotFound();
            }
            ViewBag.formMode = "details";
            return View(bankRepresentative);
        }

        // GET: BankRepresentatives/Create
        public ActionResult Create()
        {
            ViewBag.BankID = new SelectList(db.Banks, "BankID", "BankName");
            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName",_app.ui.current.User.Employee.BranchID);
            //  ViewBag.EmpID = new SelectList(db.UsersInfoes, "UserID", "Name_f");
            BankRepresentative brep = new BankRepresentative();
            brep.UsersInfo = new UsersInfo(true);
            ViewBag.formMode = "create";
            return View(brep);
        }

        // POST: BankRepresentatives/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( BankRepresentative bankRepresentative)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    UsersInfo u = new UsersInfo(true);
                    bankRepresentative.UsersInfo.Photo = _app.ui.photo.replacePhoto(u.Photo, Request.Files);
                    bankRepresentative.UsersInfo.ActiveAccount = true;
                    // bankRepresentative.UsersInfo.RoleID = 3;

                    if (_app.ui.current.User.Employee.BranchID != 1)
                    { bankRepresentative.BranchID = _app.ui.current.User.Employee.BranchID; }


                    db.BankRepresentatives.Add(bankRepresentative);
                    db.SaveChanges();
                    _app.ui.msg.Show = "تمت الاضافه بنجاح";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception) { }
            
            ViewBag.BankID = new SelectList(db.Banks, "BankID", "BankName", bankRepresentative.BankID);
            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName", bankRepresentative.BranchID);
            ViewBag.formMode = "create";
            return View(bankRepresentative);
        }

        // GET: BankRepresentatives/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankRepresentative bankRepresentative = db.BankRepresentatives.Find(id);
            if (bankRepresentative == null)
            {
                return HttpNotFound();
            }
            ViewBag.BankID = new SelectList(db.Banks, "BankID", "BankName", bankRepresentative.BankID);
            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName", bankRepresentative.BranchID);
            ViewBag.formMode = "edit";
            return View(bankRepresentative);
        }

        // POST: BankRepresentatives/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( BankRepresentative bankRepresentative)
        {
            if (ModelState.IsValid)
            {


                try
                {
                    var oldrep = db.BankRepresentatives.Find(bankRepresentative.UsersInfo.UserID);

                    oldrep.UsersInfo.Name_f = bankRepresentative.UsersInfo.Name_f;
                    oldrep.UsersInfo.Name_l =  bankRepresentative.UsersInfo.Name_l;
                    oldrep.UsersInfo.Name_m = bankRepresentative.UsersInfo.Name_m;
                    oldrep.UsersInfo.PhoneNumber = bankRepresentative.UsersInfo.PhoneNumber;
                    oldrep.UsersInfo.Photo = _app.ui.photo.replacePhoto(oldrep.UsersInfo.Photo, Request.Files);
                    oldrep.BankID = bankRepresentative.BankID;

                    if (_app.ui.current.User.Employee.BranchID != 1)
                    { bankRepresentative.BranchID = _app.ui.current.User.Employee.BranchID; }
                    else
                    { oldrep.BranchID = bankRepresentative.BranchID ;}
               
                    oldrep.UsersInfo.ActiveAccount = bankRepresentative.UsersInfo.ActiveAccount;
                    bankRepresentative.UsersInfo.Photo = oldrep.UsersInfo.Photo;



                    db.Entry(oldrep).State = EntityState.Modified;

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {

                }

               
            }
            ViewBag.BankID = new SelectList(db.Banks, "BankID", "BankName", bankRepresentative.BankID);
            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName", bankRepresentative.BranchID);
            ViewBag.formMode = "edit";

            return View(bankRepresentative);
        }


        public ActionResult RestPassword(int id)

        {
            var br = db.BankRepresentatives.Find(id);
            //em.UsersInfo.Password = "12345678";
            //db.SaveChanges();
            _app.model ui = new _app.model();
            ui.restPassword(id, "123456789");

            _app.ui.msg.Show = "تم اعاده كلمه المرور الي " + br.UsersInfo.Password;

            return RedirectToAction("Edit", new { id = br.UsersInfo.UserID });
            // return View();
        }



        // GET: BankRepresentatives/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankRepresentative bankRepresentative = db.BankRepresentatives.Find(id);
            if (bankRepresentative == null)
            {
                return HttpNotFound();
            }
            ViewBag.formMode = "details";
            return View(bankRepresentative);
        }

        // POST: BankRepresentatives/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BankRepresentative bankRepresentative = db.BankRepresentatives.Find(id);
            db.BankRepresentatives.Remove(bankRepresentative);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
