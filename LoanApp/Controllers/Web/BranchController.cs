﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;
using System.Net;

namespace LoanApp.Controllers
{
    public class BranchController : Controller
    {
        private LoanAppDBEntities db = new LoanAppDBEntities();
        // GET: Branch
        public ActionResult Index()
        {
            return View(db.Branchs.ToList());
        }

        // GET: Branch/Details/5
        public ActionResult Details(int id)
        {
            #pragma warning disable CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            if (id == null)
            #pragma warning restore CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var br = db.Branchs.Find(id);
            if (br == null)
            {
                return HttpNotFound();
            }


            return View(br);
        }

        // GET: Branch/Create
        public ActionResult Create()

        {
            Branch b = new Branch();
            //var emps = db.Employees.Where(x => db.Branchs.Any(y => x.EmpID != y.BranchMangerUserID)).ToList();
            //ViewBag.BranchMangerUserID = new SelectList(emps, "EmpID", "FullName", null) ;
            return View(b);
        }

        // POST: Branch/Create
        [HttpPost]
        public ActionResult Create(Branch b)
        {
            try
            {
                
                b.lat = Request.Form["gmap0-t"];
                b.lng = Request.Form["gmap0-g"];
                b.Active = true;

                db.Branchs.Add(b);
                db.SaveChanges();


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Branch/Edit/5
        public ActionResult Edit(int id)
        {
            var b = db.Branchs.Find(id);
            var emps = db.Employees.Where(x => x.BranchID == id ).ToList();
            //if (b.BranchMangerUserID != null) {
            //     emps.Add(b.BranchManger.Employee);
            //}

            var q = from x in emps select new  { EmpID = x.EmpID, FullName = x.UsersInfo.FullName };
            ViewBag.BranchMangerUserID = new SelectList(q, "EmpID", "FullName", b.BranchMangerEmpID );
            return View(b);
        }

        // POST: Branch/Edit/5
        [HttpPost]
        public ActionResult Edit(Branch br)
        {
            try
            {
                // TODO: Add update logic here

                var oldBr = db.Branchs.Find(br.BranchID);
                oldBr.BranchName = br.BranchName;
                oldBr.Active = br.Active;
                oldBr.lat = Request.Form["gmap"+ br.BranchID.ToString()+"-t"];
                oldBr.lng = Request.Form["gmap" + br.BranchID.ToString() + "-g"];
                oldBr.BranchCode = br.BranchCode;
                oldBr.BranchMangerEmpID = br.BranchMangerEmpID;
                db.Entry(oldBr).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Branch/Delete/5
        public ActionResult Delete(int id)
        {
            var br = db.Branchs.Find(id);
            return View(br);
        }

        // POST: Branch/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed (int id)
        {
            try
            {
                // TODO: Add delete logic here
                var b = db.Branchs.Find(id);
                db.Branchs.Remove(b);

                db.SaveChanges();



                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
