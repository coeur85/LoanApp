﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Controllers.Web
{
   
    public class SystemSetupController : Controller
    {
        // GET: SystemSetup
        LoanAppDBEntities db = new LoanAppDBEntities();
        public ActionResult MobileAppWellcomeScreen()
        {
            var l = db.SystemSetups.Where(x => x.Code == (int) _app.SystemSetup.MobileAppPhotos).ToList();

            return View(l);
        }


        public ActionResult ContactUsNumbers()
        {
            var u = db.SystemSetups.Where(x => x.Code == (int)_app.SystemSetup.ContactUs).ToList();

            return View(u);
        }


        [HttpPost]
        public ActionResult ContactUsNumbers(List<SystemSetup> ss)
        {

          
            var u = db.SystemSetups.Where(x => x.Code == (int)_app.SystemSetup.ContactUs).ToList();
            u.FirstOrDefault(x=> x.SID  == 4).Value = Request.Form["v_4"];
            u.FirstOrDefault(x => x.SID == 5).Value = Request.Form["v_5"];
            u.FirstOrDefault(x => x.SID == 6).Value = Request.Form["v_6"];
            db.SaveChanges();
            _app.ui.msg.Show = "تم الحفظ بنجاح";

            return RedirectToAction("ContactUsNumbers") ;
        }
    }
}