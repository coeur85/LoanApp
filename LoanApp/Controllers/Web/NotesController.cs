﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Controllers.Web
{
    
    public class NotesController : Controller
    {
        // GET: Notes

        LoanApp.Models.DataBaseModel.LoanAppDBEntities db = new Models.DataBaseModel.LoanAppDBEntities();




        // GET: Commitments/Create
        public ActionResult Create(int id)
        {

            Note  n = new LoanApp.Models.DataBaseModel.Note() ;
            n.LoansRequest = db.LoansRequests.FirstOrDefault(x => x.LoanID == id);
            n.LoanID = n.LoansRequest.LoanID;
            return View(n);
        }

        // POST: Commitments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Note note)
        {
            if (ModelState.IsValid)
            {

                note.EmpID = _app.ui.current.User.UserID;
                note.NoteDate = _app.ui.DateNow;


                db.Notes.Add(note);
                //var lo = db.LoansRequests.FirstOrDefault(x => x.LoanID == note.LoanID);

                db.SaveChanges();

                return RedirectToAction("Details","LoanRequest", new { id = note.LoanID });
            }


            return View(note);
        }




    }
}