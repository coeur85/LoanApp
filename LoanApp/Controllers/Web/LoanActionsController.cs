﻿using LoanApp.Models.DataBaseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rotativa;
using LoanApp.Models.Classes;


namespace LoanApp.Controllers
{
    public class LoanActionsController : Controller
    {
        LoanAppDBEntities db = new LoanAppDBEntities();
        Send.Notifications no = new Send.Notifications();
        public ActionResult MoveToBranch(int id)
        {

            var l = db.LoansRequests.Find(id);
            if (l.StatusID != 1) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            ViewBag.BranchID = new SelectList(db.Branchs.Where(x => x.BranchID != 1), "BranchID", "BranchName");
            return View(l);
        }
        [HttpPost]
        public ActionResult MoveToBranch(FormCollection collection)
        {

            var l = db.LoansRequests.Find(Convert.ToInt32(Request.Form["LoanID"]));
            var br = db.Branchs.Find(Convert.ToInt32(Request.Form["BranchID"]));
         //   l.StatusID = 4;
            l.BranchID = br.BranchID;
            l.Branch = br;
           

            _app.Audits.NewForStatus4(_app.ui.current.User, l, br);

            //db.LoansRequests.Attach(lr);
            db.SaveChanges();
            var msg = "لقد تم تحويل طلبكم الي فرع ";
            msg += (" " + br.BranchName);

            NotifyOwner(l, msg, true);
           
            return RedirectToAction("Details","LoanRequest", new { id = l.LoanID });
        }



        public ActionResult MoveToBank(int id)
        {

            var l = db.LoansRequests.Find(id);
            if (l.StatusID != 4) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            var q = from br in l.Bank.BankRepresentatives.Where(x=> x.BranchID == l.BranchID) select new { br.UsersInfo.UserID , br.UsersInfo.FullName };
            ViewBag.UserID = new SelectList(q.ToList(), "UserID", "FullName");



            return View(l);
        }
        [HttpPost]
        public ActionResult MoveToBank(FormCollection collection)
        {

            var l = db.LoansRequests.Find(Convert.ToInt32(Request.Form["LoanID"]));
            var bnk = db.BankRepresentatives.Find(Convert.ToInt32(Request.Form["UserID"]));
         //   l.StatusID = 5;
            l.BankRepID = bnk.UsersInfo.UserID;

            _app.Audits.NewForStatus5(_app.ui.current.User, l, bnk);

            var head = "لقد تم ارسال طلب جديد اليكم";
            var body = "طلب رقم " + l.LoanID.ToString(); ;
            no.SendNotificationForLoan(bnk.UsersInfo, head,body,l);
            db.SaveChanges();

            return RedirectToAction("Details","LoanRequest", new { id = l.LoanID });
        }



        public ActionResult AddBankData(int id)
        {

            var l = db.LoansRequests.Find(id);
            if (l.StatusID != 5) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
           
            return View(l);
        }
        [HttpPost]
        public ActionResult AddBankData(FormCollection collection)
        {

            var l = db.LoansRequests.Find(Convert.ToInt32(Request.Form["LoanID"]));
            l.MaxFunds = Convert.ToDecimal(Request.Form["MaxFunds"]);
            l.Salary3Month = Convert.ToDecimal(Request.Form["Salary3Month"]);
            l.EarlyPayment = Convert.ToDecimal(Request.Form["EarlyPayment"]);
            l.StatusID = 6;


            
            _app.Audits.NewForStatus6(_app.ui.current.User, l);
           
            db.SaveChanges();

            return RedirectToAction("Details", "LoanRequest", new { id = l.LoanID });
        }

        public ActionResult AddLoanFundData(int id)
        {

            var l = db.LoansRequests.Find(id);
            if (l.StatusID != 6) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            ViewBag.BankID = new SelectList(db.Banks, "BankID", "BankName");

            return View(l);
        }
        [HttpPost]
        public ActionResult AddLoanFundData(FormCollection collection)
        {

            var l = db.LoansRequests.Find(Convert.ToInt32(Request.Form["LoanID"]));
            l.BankAccount = Request.Form["BankAccount"];
            l.ActualFunds = Convert.ToDecimal(Request.Form["ActualFunds"]);
            l.FundBankID = Convert.ToInt32(Request.Form["BankID"]);
            // l.StatusID = 7;



            //Audit au = new Audit();

            //au.AuditDate = DateTimeOffset.UtcNow.DateTime.ToLocalTime();
            //au.UserID = _app.ui.current.User.UserID;
            //au.AuditName = "تم استيفاء بيانات القرض من  قبل موظف الفرع ";

            //au.LoanID = l.LoanID;
            //db.Audits.Add(au);

            _app.Audits.NewForStatus7(_app.ui.current.User, l);

            //db.LoansRequests.Attach(lr);
            db.SaveChanges();

            return RedirectToAction("Details", "LoanRequest", new { id = l.LoanID });
        }


        public ActionResult AcceptOrDeclineLoan(int id)
        {
            var l = db.LoansRequests.Find(id);
            if (l.StatusID == 7 || l.StatusID == 10 || l.StatusID == 2 || l.StatusID == 1) { return View(l);  }
            return  new HttpStatusCodeResult(HttpStatusCode.BadRequest);;
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Accept")]
        public ActionResult Accept(FormCollection collection)
        {
            var l = db.LoansRequests.Find(Convert.ToInt32(Request.Form["LoanID"]));
            _app.Audits.NewForStatus9(_app.ui.current.User, l);

            
            db.Entry(l).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            var msg = "سعدنا بتواصلكم ونسعد بخدمتكم، تمت الموافقه علي  ";

            NotifyOwner(l, msg, false);


            return RedirectToAction("Details", "LoanRequest", new { id = l.LoanID });
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Decline")]
        public ActionResult Decline(FormCollection collection)
        {
            var l = db.LoansRequests.Find(Convert.ToInt32(Request.Form["LoanID"]));
            _app.Audits.NewForStatus8(_app.ui.current.User, l);
            string declineReason = "";
            if (Request.Form["DeclineReason"] != null) {declineReason= Request.Form["DeclineReason"].ToString(); }

             

            if (!string.IsNullOrEmpty(declineReason)) { l.DeclineReason = declineReason; }

            //l.StatusID = 8;
            //l.Audits.Add(new Audit
            //{
            //    AuditDate = DateTimeOffset.UtcNow.DateTime.ToLocalTime(),
            //    LoanID = l.LoanID,
            //    UserID = _app.ui.current.User.UserID,
            //    AuditName = "تم رفض الطلب من قبل مدير التظام"
            //});
            db.Entry(l).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            var msg = "لقد تم رفض طلبكم ";

            NotifyOwner(l, msg, true);

            return RedirectToAction("Details", "LoanRequest", new { id = l.LoanID });
        }

        
        public ActionResult Print(int id)
        {

            //  var l = db.LoansRequests.Find(id);
            //  if (l.StatusID != 9) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

             return new ActionAsPdf ("PrintLoan", new { id = id }) { FileName="PDF_File.pdf" };
           // return RedirectToAction("PrintLoan", new { id = id });
           // return View();
        }
        [AllowAnonymous]
        public ActionResult PrintLoan(int id)
        {
            var l = db.LoansRequests.Find(id);
            if (l.StatusID != 9) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            

            return View(l);
        }


        public ActionResult DeclineLoanByBranch(int id)
        {

            var l = db.LoansRequests.Find(id);
            if (l.StatusID == 4  || l.StatusID ==6) { return View(l);  }





            return new HttpStatusCodeResult(HttpStatusCode.BadRequest); ;
        }
        [HttpPost]
        public ActionResult DeclineLoanByBranch(FormCollection collection)
        {

            var l = db.LoansRequests.Find(Convert.ToInt32(Request.Form["LoanID"])); 
            l.StatusID = 10;
            l.DeclineReason = Request.Form["DeclineReason"];
            //Audit au = new Audit();

            //au.AuditDate = DateTimeOffset.UtcNow.DateTime.ToLocalTime();
            //au.UserID = _app.ui.current.User.UserID;
            //au.AuditName = "تم رفض الطلب من قبل موظف الفرع";

            //au.LoanID = l.LoanID;
            //db.Audits.Add(au);
            _app.Audits.NewForStatus10(_app.ui.current.User, l);
            //db.LoansRequests.Attach(lr);


            var msg = "لقد تم رفض طلبكم ";

            NotifyOwner(l, msg, true);

            db.SaveChanges();

            return RedirectToAction("Details", "LoanRequest", new { id = l.LoanID });
        }


        public ActionResult Edit(int id)
        {

            var l = db.LoansRequests.Find(id);
            if (!_app.LoanReq.CanBeEditedStatus().Contains(l.StatusID)) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            ViewBag.BankID = new SelectList(db.Banks, "BankID", "BankName", l.BankID);
            ViewBag.CustomerSelectedBranchID = new SelectList(db.Branchs, "BranchID", "BranchCode", l.CustomerSelectedBranchID);
            return View(l);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "SaveAndReEvaluate")]
        public ActionResult SaveAndReEvaluate(LoansRequest l)
        {

            var dbl = db.LoansRequests.FirstOrDefault(x => x.LoanID == l.LoanID);
           UpdateLaon(l , dbl);
            if (dbl.StatusID == 0 || dbl.StatusID == 2 || dbl.StatusID == 3) { _app.LoanReq.LoanEquation(dbl); }

            db.SaveChanges();
            return RedirectToAction("Details", "LoanRequest", new { id = l.LoanID });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "SaveAndSendToSameBracnch")]
        public ActionResult SaveAndSendToSameBracnch(LoansRequest l)
        {
            var dbl = db.LoansRequests.FirstOrDefault(x => x.LoanID == l.LoanID);
            UpdateLaon(l, dbl);

            if (dbl.StatusID == 10) { _app.Audits.NewForStatus4(_app.ui.current.User , dbl,dbl.Branch); }

            db.SaveChanges();
            return RedirectToAction("Details", "LoanRequest", new { id = l.LoanID });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "SaveAndChangeBranch")]
        public ActionResult SaveAndChangeBranch(LoansRequest l)
        {
            var dbl = db.LoansRequests.FirstOrDefault(x => x.LoanID == l.LoanID);
            UpdateLaon(l, dbl);
            if (dbl.StatusID == 10) { _app.Audits.NewForStatus1(_app.ui.current.User, dbl); }

            db.SaveChanges();
            return RedirectToAction("Details", "LoanRequest", new { id = l.LoanID });
        }



        public ActionResult EmpTransfer(int id)
        {
            var u = _app.ui.current.User;
            var l = u.Employee.MyLoanRequest.FirstOrDefault(x => x.LoanID == id);
            if(l == null)
            {
                return HttpNotFound();
            }

            var emps = from x in db.Branchs.Find(l.BranchID).Employees
                       select new { BranchEmpID = x.EmpID, FullName = x.UsersInfo.FullName };

            ViewBag.BranchEmpID = new SelectList(emps, "BranchEmpID", "FullName", l.BranchEmpID);

            return View(l);
        }



        [HttpPost]
        public ActionResult EmpTransfer(LoansRequest l)
        {

            if (ModelState.IsValid) {
                var old = db.LoansRequests.Find(l.LoanID);
                old.BranchEmpID = l.BranchEmpID;
                db.Entry(old).State = System.Data.Entity.EntityState.Modified;
                _app.Audits.NewForEmptransfer(_app.ui.current.User, old);
                db.SaveChanges();
                _app.ui.msg.Show = "تم تحويل الطلب بنجاح";

            }

             
            return RedirectToAction("Details", "LoanRequest", new { id = l.LoanID }) ;
        }


        private void NotifyOwner(LoansRequest l , string body , bool addloanNumber)
        {

            var head = "تم تحديث بيانات الطلب";
            if (addloanNumber) { body += (" الطلب رقم " + l.LoanID.ToString()); }
           
            if (l.SalesRepresentative != null)
            {
                no.SendNotificationForLoan(l.SalesRepresentative.UsersInfo,head, body,l );
            }
            else { no.SendNotificationForLoan(l.Customer.UsersInfo,head, body,l); }


        }
        private void UpdateLaon(LoansRequest PostedLoan, LoansRequest oldloan)
        {
          //  var oldloan = db.LoansRequests.FirstOrDefault(x=> x.LoanID ==PostedLoan.LoanID);
            oldloan.BankID = PostedLoan.BankID;
            oldloan.Salary = PostedLoan.Salary;
            oldloan.CustomerSelectedBranchID = PostedLoan.CustomerSelectedBranchID;
            _app.Audits.NewForEmpEdit(_app.ui.current.User, oldloan);
          //  return oldloan;


        }

    }
}
