﻿using LoanApp.Models.DataBaseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoanApp.Controllers
{
    public class ProfileController : Controller
    {
        private LoanAppDBEntities db = new LoanAppDBEntities();
        // GET: Profile
        public ActionResult Index()
        {

            return View(_app.ui.current.User);
        }

        [HttpPost]
        public ActionResult Save(UsersInfo u)
        {
            var oldusr = db.UsersInfoes.Find(u.UserID);
            oldusr.Name_f = u.Name_f;
            oldusr.Name_l = u.Name_l;
            oldusr.Name_m = u.Name_m;
            oldusr.Password = u.Password;
            oldusr.Photo = _app.ui.photo.replacePhoto(oldusr.Photo, Request.Files);
            db.Entry(oldusr).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            _app.ui.msg.Show = "تم حفظ التعيرات بنجاح";

          return  RedirectToAction("index", new { id = oldusr.UserID });
           // return View();
        }
    }
}