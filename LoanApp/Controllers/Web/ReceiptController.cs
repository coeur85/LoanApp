﻿using LoanApp.Models.DataBaseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rotativa;

namespace LoanApp.Controllers
{
    public class ReceiptController : Controller
    {
        private LoanAppDBEntities db = new LoanAppDBEntities();
        // GET: Receipt
        public ActionResult Index()
        {
            var rec = db.Receipts.ToList();
            return View(rec);
        }

        // GET: Receipt/Details/5
        public ActionResult Details(int id)

        {

            ViewBag.formMode = "Details";
            var r = db.Receipts.Find(id);
            if (r == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            return View(r);
        }

        // GET: Receipt/Create
        public ActionResult Create()
        {

            var qsr = from sp in db.SalesRepresentatives.ToList() select new { sp.UsersInfo.UserID, sp.UsersInfo.FullName };
            ViewBag.SalesRepID = new SelectList(qsr.ToList(), "UserID", "FullName");

            ViewBag.BranchID = new SelectList(db.Branchs, "BranchID", "BranchName");

            var qemp = from em in db.Employees.ToList() select new { em.UsersInfo.FullName, em.UsersInfo.UserID };
            ViewBag.EmpID = new SelectList(qemp, "UserID", "FullName");
            var emp = db.Employees.Find(_app.ui.current.User.UserID);
            Receipt r = new Receipt();
            r.CreatedEmployee = emp;
            ViewBag.formMode = "Create";
            return View(r);


            ;
        }

        // POST: Receipt/Create
        [HttpPost]
        public ActionResult Create(Receipt re)
        {
            //try
            //{
            // TODO: Add insert logic here

            re.CreateEmpID = _app.ui.current.User.UserID;
            re.DateAdded = _app.ui.DateNow;
            db.Receipts.Add(re);
            db.SaveChanges();

            return RedirectToAction("Details", new { id = re.ReceiptID });
            //}
            //catch
            //{
            //    return View();
            //}
        }



        // GET: Receipt/Delete/5
        public ActionResult Delete(int id)
        {
            var r = db.Receipts.Find(id);
            if (r == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            ViewBag.formMode = "Details";
            return View(r);
        }

        // POST: Receipt/Delete/5
       
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed (int id)
        {
            try
            {

                var r = db.Receipts.Find(id);
                db.Receipts.Remove(r);
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        
        public ActionResult Print(int id)
        {
            
            return new ActionAsPdf("PrintRecepit", new { id = id }) { FileName="PDF_File.pdf" } ;
        }
        [AllowAnonymous]
        public ActionResult PrintRecepit(int id)
        {
            ViewBag.formMode = "Details";
            var r = db.Receipts.Find(id);
            if (r == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            return View(r);
           
        }
    }
}
