﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Controllers.Web
{
    public class CommitmentsController : Controller
    {
        private LoanAppDBEntities db = new LoanAppDBEntities();

        // GET: Commitments
        public ActionResult Index(int id)
        {
            //  var commitments = db.Commitments.Include(c => c.LoansRequest).Where(x=> x.LoanID == id);
           

            var l = db.LoansRequests.Include(XmlReadMode => XmlReadMode.Commitments).FirstOrDefault(x => x.LoanID == id);
            if (!_app.LoanReq.CanBeEditedStatus().Contains(l.StatusID)) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (l.StatusID == 10 && _app.ui.current.User.Employee.BranchID != 1) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

           
            return View(l);
        }


        // GET: Commitments/Create
        public ActionResult Create(int id)
        {

            Commitment c = new Commitment();
            c.LoansRequest = db.LoansRequests.FirstOrDefault(x => x.LoanID == id);
            c.LoanID = c.LoansRequest.LoanID;
            return View(c);
        }

        // POST: Commitments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Commitment commitment)
        {
            if (ModelState.IsValid)
            {

                db.Commitments.Add(commitment);
                var lo = db.LoansRequests.FirstOrDefault(x => x.LoanID == commitment.LoanID);
                _app.Audits.NewForCommitmentAddNew(_app.ui.current.User,lo);

                db.SaveChanges();

                return RedirectToAction("Index",new { id = commitment.LoanID });
            }

           
            return View(commitment);
        }

        // GET: Commitments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Commitment commitment = db.Commitments.Find(id);
            if (commitment == null)
            {
                return HttpNotFound();
            }
        
            return View(commitment);
        }

        // POST: Commitments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Commitment commitment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(commitment).State = EntityState.Modified;
                var lo = db.LoansRequests.FirstOrDefault(x => x.LoanID == commitment.LoanID);
                _app.Audits.NewForCommitmentUpdate(_app.ui.current.User, lo);
                db.SaveChanges();
                return RedirectToAction("Index", new { id= commitment.LoanID });
            }
            return View(commitment);
        }

        // GET: Commitments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Commitment commitment = db.Commitments.Find(id);
            if (commitment == null)
            {
                return HttpNotFound();
            }
            return View(commitment);
        }

        // POST: Commitments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            Commitment commitment = db.Commitments.Find(id);
            var l = commitment.LoanID;
            var lo = db.LoansRequests.FirstOrDefault(x => x.LoanID == l);
            _app.Audits.NewForCommitmentDelete(_app.ui.current.User, lo);
            db.Commitments.Remove(commitment);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = l });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
