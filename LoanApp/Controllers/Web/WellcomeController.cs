﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoanApp.Controllers.Web
{
    [AllowAnonymous]
    public class WellcomeController : Controller
    {
        // GET: Wellcome
        public ActionResult Index()
        {
            return View();
        }
    }
}