﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {

        // GET: Login
        LoanAppDBEntities db = new LoanAppDBEntities();
        public ActionResult Index()
        {

            LoginModel lm = new LoginModel();
            return View(lm);
        }

        [HttpPost]
        public ActionResult index(LoginModel lm)
        {

            var u = db.Employees.Where(x => x.UsersInfo.Password ==
            lm.Password && x.UsersInfo.PhoneNumber == lm.PhoneNumber).FirstOrDefault();

            if (u != null) {

                if (u.UsersInfo.ActiveAccount == true)
                {
                    _app.ui.current.User = u.UsersInfo;

                    string url = (string)Request.QueryString["returnUrl"];

                    if (!string.IsNullOrEmpty(url)) { Response.Redirect(url); return null; }
                    return RedirectToAction("index", "Home");

                }
                else { lm.Message = "حسابك غير مفعل برجاء الاتصال بمدير النظام"; }


            }
            else {  lm.Message = "اسم المستخدم او كلمه المرور عير صحيح"; }

            return View(lm);
        }


        public ActionResult LogOut()
        {
            _app.ui.current.User = null; Session["UserMenu"] = null; return RedirectToAction("index");

        }
    }
}