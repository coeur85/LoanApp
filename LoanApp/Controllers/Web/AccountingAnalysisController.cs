﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Controllers.Web
{
    public class AccountingAnalysisController : Controller
    {
        // GET: AccountingAnalysis

        LoanAppDBEntities db = new LoanAppDBEntities();
        public ActionResult Index()
        {

            WebAdo.AccountingAnalysis aa = new WebAdo.AccountingAnalysis();

            aa.receipts = db.Receipts.ToList();


            aa.Loans = db.LoansRequests.Where(x => x.StatusID == 9 && x.SalesRepID != null).ToList();


            return View(aa);
        }
    }
}