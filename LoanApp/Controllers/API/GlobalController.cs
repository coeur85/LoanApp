﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Controllers.API
{
    public class GlobalController : ApiController
    {


        LoanAppDBEntities db = new LoanAppDBEntities();
        public IHttpActionResult UploadMobileScreenPhoto( WebAdo.MobileWelcomeScreen model)
        {
            try
            {

                var p = db.SystemSetups.FirstOrDefault(x => x.SID == model.dbid);
                p.Value = model.Photo;
                db.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {

                return InternalServerError(e);
            }




        }



        
      
    }
}
