﻿using LoanApp.Models.DataBaseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoanApp.Controllers.API.SalesRepApp
{
    public class SalesRepController : ApiController
    {


        _app.Validation v = new _app.Validation();
        LoanAppDBEntities db = new LoanAppDBEntities();
        ResponoseMEssage r = new ResponoseMEssage();
        public IHttpActionResult ChangeStatus(MobileADO.NewLoanID model)
        {
            try
            {
                ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
                ex = v.SessionValid(model.SessionID.ToString());
                if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }

                var sales = db.MobileSessions.Where(x => x.SessionID.ToString() == model.SessionID).FirstOrDefault().UsersInfo.SalesRepresentative;
                var l = sales.LoansRequests.Where(x => x.LoanID == model.CurrentLoanID).FirstOrDefault();

                if (l.StatusID == 0)
                {

                    //  l.Audits.Add(new Audit { AuditDate = DateTimeOffset.UtcNow.DateTime.ToLocalTime(), AuditName = "المندوب قام بتاكيد البيانات", UserID = l.CustomerID });
                    _app.Audits.NewForLoanConfrimed(sales.UsersInfo, l);
                    _app.LoanReq.LoanEquation(l);
                    db.SaveChanges();
                    return Ok(new ResponoseMEssage { Message = "تم ارسال الطلب الي الموظفين المسؤلين وسوف يتم اخطارك بالمستجدات قريبا", status = true });
                }
                else if (l.StatusID == 3) { return Ok(new ResponoseMEssage { Message = "تم ارسال الطلب الي الموظفين المسؤلين وسوف يتم اخطارك بالمستجدات قريبا", status = true }); }

                else { ex = v.NewEx("هذا الطلب جاري العمل عليه ولايمكن التعديل في بياناته", _app.Validation.ErrorType.Loan); return Ok(ex); }

            }
            catch (Exception e)
            {

                return Ok(e);
            }




        }

        public IHttpActionResult UpdateOrAddNewLoan(MobileLoanReuest model)
        {

            ResponoseErrorMEssage ex = new ResponoseErrorMEssage();

            ex = v.SessionValid(model.SessionID);
            if (ex.errors.Count > 0) { return Ok(ex); }

            ex = v.PhoneNumer(model.Customer.PhoneNumber);
            if (ex.errors.Count > 0) { return Ok(ex); }


            var sales = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID).UsersInfo.SalesRepresentative;
            if (sales == null) { ex = v.NewEx("مسموح فقط لمندوبين المبيعات اضافه طلبات قروض للغير ، برجاء الاتصال بمدير التظام",
                _app.Validation.ErrorType.Account); return Ok(ex); }


            ex = v.LoanValied(model.LoansRequest);
            if (ex.errors.Count > 0) { return Ok(ex); }

            LoansRequest l = new LoansRequest();
            l = db.LoansRequests.FirstOrDefault(x => x.LoanID == model.LoansRequest.LoanID);
            bool newLoan = false;
            if (l != null)
            {
                if (l.StatusID != 3) { ex = v.NewEx("هذا الطلب جار العمل عليه ولا يمكن تعديله", _app.Validation.ErrorType.Loan); return Ok(ex); }
                if (l.SalesRepresentative != sales) { ex = v.NewEx("لايمكن تعديل طلب لست انت مندوب عنه", _app.Validation.ErrorType.Loan); return Ok(ex); }
                AddSucessMsg("تم تحديث بيانات القرض");
                newLoan = false;
            }
            else { l = new LoansRequest(); AddSucessMsg("تم اضافه بيانات قرض جديد"); newLoan = true; }

            var lon = model.LoansRequest;

            foreach (var com in lon.Commitments)
            {
                l.Commitments.Add(new Commitment { CommitmentName = com.CommitmentName, CommitmentValue = com.CommitmentValue });
            }

        //    l.Funds = lon.Funds;
            l.Salary = lon.Salary;
            l.SalesRepresentative = sales;
            l.BankID = model.LoansRequest.BankID;
            l.SalesRepID = sales.SalesRepID;

            if (string.IsNullOrEmpty(lon.CustomerBranchID)) { l.CustomerSelectedBranchID = null; }
            else { l.CustomerSelectedBranchID = Convert.ToInt32(lon.CustomerBranchID); }

            if (lon.LoanID > 0) { _app.Audits.NewForSalesrEdit(sales.UsersInfo, l); }
            else { _app.Audits.NewForStatus0(sales.UsersInfo, l); }

            bool newCust = false;
            var cus = db.Customers.FirstOrDefault(x => x.UsersInfo.PhoneNumber == model.Customer.PhoneNumber);
            if (cus == null) { cus = new Customer { UsersInfo = new UsersInfo(true) }; AddSucessMsg("تم اضافه عميل جديد"); newCust = true; }



            if (newCust == false)
            {

               if (cus.UsersInfo.Name_f != model.Customer.Name_f ||
               cus.UsersInfo.Name_l != model.Customer.Name_l ||
               cus.UsersInfo.Name_m != model.Customer.Name_m ||
               cus.NationalID != model.Customer.NationalID ||
               cus.CityID != model.Customer.CityID ||
               cus.JobID != model.Customer.JobID)

                {


                    AddSucessMsg("تم تحديث بيانات العميل");

                }


            }





            if (newCust)
            {

                cus.UsersInfo.Name_f = model.Customer.Name_f;
                cus.UsersInfo.Name_l = model.Customer.Name_l;
                cus.UsersInfo.Name_m = model.Customer.Name_m;

                cus.NationalID = model.Customer.NationalID;
                cus.CityID = model.Customer.CityID;
                cus.JobID = model.Customer.JobID;

              
            }


            else
            {
                if (!string.IsNullOrEmpty(model.Customer.Name_f)) { cus.UsersInfo.Name_f = model.Customer.Name_f; }
                if (!string.IsNullOrEmpty(model.Customer.Name_l)) { cus.UsersInfo.Name_f = model.Customer.Name_l; }
                if (!string.IsNullOrEmpty(model.Customer.Name_m)) { cus.UsersInfo.Name_f = model.Customer.Name_m; }
                if (model.Customer.CityID.HasValue) { cus.CityID = model.Customer.CityID; }
                if (model.Customer.JobID.HasValue) { cus.JobID = model.Customer.CityID; }


            }






            bool complte = false;

            if (!string.IsNullOrEmpty(cus.UsersInfo.Name_f) &&
                !string.IsNullOrEmpty(cus.UsersInfo.Name_l) &&
                !string.IsNullOrEmpty(cus.UsersInfo.Name_m) &&
                !string.IsNullOrEmpty(cus.NationalID) &&
                cus.CityID.HasValue &&
                cus.JobID.HasValue )
                
            {

                complte = true;

            }
           

            if (newCust)
            {
                cus.UsersInfo.PhoneNumber = model.Customer.PhoneNumber;
               // cus.UsersInfo.RoleID = 3;
                db.Customers.Add(cus);
               
            }
            db.SaveChanges();

            if (complte) { l.StatusID = 0; }
            else { l.StatusID = 3; }

            l.CustomerID = cus.CustomerID;
            l.Customer = cus;

            if (newLoan) { l.RequestDate = _app.ui.DateNow; db.LoansRequests.Add(l); }

            db.SaveChanges();

            if (!newCust && complte)
            {
                var cl = cus.LoansRequests.Where(x => x.StatusID == 3).ToList();
                foreach (var ld in cl)
                {
                    _app.LoanReq.LoanEquation(ld);
                    _app.Audits.NewForSalesrUpdateOldLoans(sales.UsersInfo, ld);
                }

                db.SaveChanges();
            }


            MobileADO.NewLoanID cms = new MobileADO.NewLoanID();
            cms.CurrentLoanID = l.LoanID;
            cms.CommitmentsTotal = l.CommitmentsTotal;
            var b = db.Banks.FirstOrDefault(x => x.BankID == l.BankID);
            cms.MaxFund = l.Salary * b.IntersetsRate;
            cms.Total = cms.MaxFund - cms.CommitmentsTotal;

            r.data = cms;
            return Ok(r);
        }



        public IHttpActionResult UpdateOrAddLoan(string value)


        {


            MobileLoanReuest model = Newtonsoft.Json.JsonConvert.DeserializeObject<MobileLoanReuest>(value);



            return UpdateOrAddNewLoan(model);


        }




        private void AddSucessMsg(string msg) { if (r.Messages == null) { r.Messages = new List<Messages>(); } r.Messages.Add( new Messages { message = msg }); }
    }
}
