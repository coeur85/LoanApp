﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
// System.Web.Mvc;
using LoanApp.Models.DataBaseModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;

namespace LoanApp.Controllers.API
{
    [AllowAnonymous]
    public class LoginController : ApiController
    {
        LoanAppDBEntities db = new LoanAppDBEntities();
        [System.Web.Http.HttpPost]
        public IHttpActionResult Login(MobileLoginModel model)
        {

            // db.Configuration.LazyLoadingEnabled = false;
            ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
            _app.Validation v = new _app.Validation();

           

            if(model == null) { ex = v.NewEx("invalid json object", _app.Validation.ErrorType.Account); return Ok(ex); }

            var u = db.UsersInfoes.Include(x=> x.Customer ).Include(x=> x.BankRepresentative)
                .Include(x=> x.SalesRepresentative).Where(x => x.PhoneNumber == model.PhoneNumber 
            && x.Password == model.Password).FirstOrDefault();

            
            if(u == null) { ex = v.NewEx("خطاء في رقم الجوال او كلمه السر", _app.Validation.ErrorType.Account); return Ok(ex); }


            if (u.Customer == null && u.SalesRepresentative == null && u.BankRepresentative == null )
            {
                ex = v.NewEx("غير مصرج لهذا الرقم الدخول التطبيق", _app.Validation.ErrorType.Account);
                return Ok(ex);
            }
            else
            {

                if(u.ActiveAccount != true) { ex = v.NewEx("حسابك غير نشط", _app.Validation.ErrorType.Account); return Ok(ex);  }


                try
                {

                    var ms = _app.mobileApp.LogUser(u, model);

                    //   MobileSession ms = new MobileSession();

                    //   ms = db.MobileSessions.Where(x => x.UsersInfo.PhoneNumber == model.PhoneNumber
                    //   && x.ExpireDate > DateTimeOffset.UtcNow.DateTime.ToLocalTime()).FirstOrDefault();


                    //   if(ms == null)
                    //   {
                    //       ms = new MobileSession();
                    //   ms.SessionID =  Guid.NewGuid();
                    //   ms.UserID = u.UserID;
                    //   ms.MobileID = model.MobileID;
                    //   ms.ExpireDate = DateTimeOffset.UtcNow.DateTime.ToLocalTime().AddDays(2);
                    //   db.MobileSessions.Add(ms);
                    //   db.SaveChanges();
                    //   }

                    //   if(u.Customer != null) { ms.AccountType = "Customer"; }
                    //   else if(u.SalesRepresentative != null) { ms.AccountType = "SalesRepresentative"; }
                    //   else if(u.BankRepresentative != null) { ms.AccountType = "BankRepresentative"; }
                    ////   db.Configuration.LazyLoadingEnabled = true;
                    //   db.MobileSessions.Remove(ms);
                    ResponoseMEssage r = new ResponoseMEssage();
                    r.data = ms;
                return Ok(r);
                }
                catch (Exception e)
                {

                    return InternalServerError(e); ;
                }

              

            }


            
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult LogOut(MobileADO.MobileSession model)
        {

            // db.Configuration.LazyLoadingEnabled = false;
            ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
            _app.Validation v = new _app.Validation();


            try
            {
                if (model == null) { ex = v.NewEx("invalid json object", _app.Validation.ErrorType.Account); return Ok(ex); }

                ex = v.SessionValid(model.SessionID.ToString());

                if (ex.errors.Count > 0) { return Ok(ex); }

                var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID == model.SessionID);
                ms.ExpireDate = _app.ui.DateNow.AddDays(-1);
                db.SaveChanges();

                ResponoseMEssage r = new ResponoseMEssage();

                r.Message = "تم تسجيل خروجك بنجاح";
                return Ok(r);
            }
            catch (Exception e)
            {

                 return Ok(e);
            }



            

        }

    }
}
