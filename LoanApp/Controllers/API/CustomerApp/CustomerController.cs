﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LoanApp.Models.DataBaseModel;
using LoanApp.Models.Classes;
using System.Data.Entity;
using System.Web.Script.Serialization;
using System.Web.Http.Results;
using Newtonsoft.Json;

namespace LoanApp.Controllers.API.CustomerApp
{
    public class CustomerController : ApiController
    {

        LoanAppDBEntities db = new LoanAppDBEntities();
        Send.TextMessage st = new Send.TextMessage();
        _app.Validation v = new _app.Validation();
      



        public IHttpActionResult SignUp(MobileLoginModel model)
        {

            try
            {
                // if (!int.TryParse(model.PhoneNumber, out var n)) { return InternalServerError(new Exception("not a vaild phone number")); }
              //  ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
               var ex = v.SingUp(model.PhoneNumber);
                if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex);   }


                Customer c = new Customer();
               c.UsersInfo = new UsersInfo(true);
                c.UsersInfo.PhoneNumber = model.PhoneNumber;
                c.RegesterDate = _app.ui.DateNow;
                var rd = new Random();
                c.ActivationCode = rd.Next(1000, 9999).ToString();
                c.ActivationCodeDate = _app.ui.DateNow;
                c.UsersInfo.ActiveAccount = false;
               

                st.SendConfirmText(c.UsersInfo.PhoneNumber, c.ActivationCode);
                db.Customers.Add(c);
                db.SaveChanges();

                //Send.TextMessage te = new Send.TextMessage();
                //te.SendConfirmText(c.UsersInfo.PhoneNumber, c.ActivationCode);
                return Ok( new ResponoseMEssage { status=true ,Message = "تم ارسال كود التفعيل عبر الرسائل النصيه بنحاح" });
            }
            catch (Exception e)
            {
                return InternalServerError(e);


            }



        }
        public IHttpActionResult ConfirmCode(MobileLoginModel model)
        {
            ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
            ex = v.ConfirmCode(model.PhoneNumber);
            if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }


            var u = db.Customers.Where(x => x.UsersInfo.PhoneNumber == model.PhoneNumber).FirstOrDefault();

          //  if (u == null) { return NotFound(); }

            if (u.ActivationCode == model.ConfirmCode)
            {
                u.UsersInfo.ActiveAccount = true;
                db.SaveChanges();
                return Ok( new ResponoseMEssage { Message ="تم تنشيط الحساب" , status=true });
            }
            else { ex = v.NewEx("كود غير سليم", _app.Validation.ErrorType.Account) ; return Ok(ex); }

        }
        public IHttpActionResult ResendCode(MobileLoginModel model)
        {

            ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
            ex = v.ConfirmCode(model.PhoneNumber);
            if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }

            var u = db.Customers.Where(x => x.UsersInfo.PhoneNumber == model.PhoneNumber).FirstOrDefault();

          //  if (u == null) { return NotFound(); }

            if (u.ActivationCodeDate.Value.AddMinutes(6) < _app.ui.DateNow)
            {
                var rd = new Random();
                u.ActivationCode = rd.Next(1000, 9999).ToString();
                u.ActivationCodeDate = _app.ui.DateNow;
                db.SaveChanges();

            }


            st.SendConfirmText(u.UsersInfo.PhoneNumber, u.ActivationCode);
            return Ok( new ResponoseMEssage { Message = "تم ارسال كود التفعيل عبر الرسائل النصيه بنحاح", status = true });

        }
        public IHttpActionResult UpdatePassword(MobileLoginModel model)
        {

            ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
            ex = v.UpdatePassword(model);
            if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }


            var c = db.Customers.Where(x => x.UsersInfo.PhoneNumber == model.PhoneNumber).FirstOrDefault();

          //  if (c == null) { return NotFound(); }
       //     if (c.UsersInfo.ActiveAccount != true) { return InternalServerError(); }
         //   else
           // {
                c.UsersInfo.Password = model.Password;
                db.SaveChanges();

                var ms = _app.mobileApp.LogUser(c.UsersInfo, model);

                return Ok( new ResponoseMEssage { Message = ms.SessionID.ToString(), status = true });

            //}
        }
        public IHttpActionResult RestPassword(MobileLoginModel model)
        {
            ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
            ex = v.RestPassword(model.PhoneNumber);
            if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }


            var c = db.Customers.Where(x => x.UsersInfo.PhoneNumber == model.PhoneNumber).FirstOrDefault();

         //   if (c == null) { return NotFound(); }
            c.UsersInfo.ActiveAccount = false;
          //  var rd = new Random();
          //  c.ActivationCode = rd.Next(1000, 9999).ToString();
            var u = new UsersInfo(true);
            c.UsersInfo.Photo = u.Photo;
          //  st.SendConfirmText(c.UsersInfo.PhoneNumber, c.ActivationCode);
            db.SaveChanges();
            //  return Ok( new ResponoseMEssage { Message= "تم ارسال كود التفعيل عبر الرسائل النصيه بنحاح", status =true });

            return ResendCode(new MobileLoginModel { PhoneNumber = model.PhoneNumber });
        }
        public IHttpActionResult UpdateOrAddNewLoan(MobileLoanReuest model)
        {
            try
            {


                ResponoseErrorMEssage ex = new ResponoseErrorMEssage();




                ex = v.SessionValid(model.SessionID);
                if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }


                var c = db.MobileSessions.Where(x => x.SessionID.ToString() == model.SessionID)
                    .FirstOrDefault().UsersInfo.Customer;

                if (c == null) { ex = v.NewEx("هذا ليس بحساب عميل ، برجاء اعاده تسجيل الدخول", _app.Validation.ErrorType.Account); return Ok(ex); }

                ex = v.CustomerValied(model.Customer);
                if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }

                c.UsersInfo.Name_f = model.Customer.Name_f;
                c.UsersInfo.Name_l = model.Customer.Name_l;
                c.UsersInfo.Name_m = model.Customer.Name_m;

                c.NationalID = model.Customer.NationalID;
                c.CityID = model.Customer.CityID;
                c.JobID = model.Customer.JobID;

                c.CityLat = model.Customer.CityLat;
                c.CityLng = model.Customer.CityLng;


                // ex = v.SessionValid(model.SessionID);
                //if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }






                //var l = c.LoansRequests.FirstOrDefault();
                //if (l == null && c.LoansRequests.Count >= 1) { ex = v.NewEx("ليس من حق العميل تقديم اكثر من طلب", _app.Validation.ErrorType.Loan) ; return Ok(ex);  }




                var lon = model.LoansRequest;

                ex = v.LoanValied(lon);
                if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }

                var l = db.LoansRequests.FirstOrDefault(x => x.LoanID == lon.LoanID);



                if (l != null)
                {
                    if (l.StatusID > 0) { return Ok(new ResponoseMEssage { Message = ("هذا الطلب جاري العمل عليه ولا يمكن تعديل بياناته"), status = false }); }
                    else
                    {
                        var lc = db.LoansRequests.Where(x => x.LoanID == l.LoanID).FirstOrDefault();
                        db.Commitments.RemoveRange(lc.Commitments);
                       
                       // lc.Commitments.AddRange(lon.Commitments);


                        lc.Commitments.Clear();
                        db.SaveChanges();
                        lc.BankID = lon.BankID;


                        foreach (var com in lon.Commitments)
                        {
                            lc.Commitments.Add( new Commitment { CommitmentName = com.CommitmentName , CommitmentValue = com.CommitmentValue  });
                        }

                   


                      //  lc.Funds = lon.Funds;
                        lc.Salary = lon.Salary;
                        //   lc.Audits.Add(new Audit() { AuditDate = DateTimeOffset.UtcNow.DateTime.ToLocalTime(), AuditName = "تم تعديل بيانات الطلب من قبل العميل", UserID = c.CustomerID });
                       

                        _app.Audits.NewForCustomerEdit(c.UsersInfo, lc);
                        db.SaveChanges();

                        MobileADO.NewLoanID cms = new MobileADO.NewLoanID();
                        cms.CurrentLoanID = lc.LoanID;
                        var bn = db.Banks.FirstOrDefault(x => x.BankID == l.BankID);
                        cms.CommitmentsTotal = l.CommitmentsTotal;
                        cms.MaxFund = l.Salary * bn.IntersetsRate;
                        cms.Total = cms.MaxFund - cms.CommitmentsTotal;
                        return Ok( new ResponoseMEssage { Message= "تم تحديث بيانات الطلب", status=true , data = cms });

                    }

                }
               

                LoansRequest lr = new LoansRequest();
                lr.CustomerID = c.CustomerID;
                lr.Customer = c;
                lr.BankID = lon.BankID;


                foreach (var com in lon.Commitments)
                {
                    lr.Commitments.Add(new Commitment { CommitmentName = com.CommitmentName, CommitmentValue = com.CommitmentValue });
                }


              //  lr.Commitments = lon.Commitments;
                lr.CustomerID = c.CustomerID;
             //   lr.Funds = lon.Funds;
                lr.StatusID = 0;
                lr.RequestDate = _app.ui.DateNow;
                lr.Salary = lon.Salary;
                if (string.IsNullOrEmpty(lon.CustomerBranchID)) { lr.CustomerSelectedBranchID = null; }
                else { lr.CustomerSelectedBranchID = Convert.ToInt32(lon.CustomerBranchID); }

                _app.Audits.NewForStatus0(c.UsersInfo, lr);

                db.LoansRequests.Add(lr);
                db.SaveChanges();

                MobileADO.NewLoanID ms = new MobileADO.NewLoanID();
                ms.CurrentLoanID = lr.LoanID;
                ms.CommitmentsTotal = lr.CommitmentsTotal;

                var b = db.Banks.FirstOrDefault(x => x.BankID == lr.BankID);
                ms.CommitmentsTotal = lr.CommitmentsTotal;
                ms.MaxFund = lr.Salary * b.IntersetsRate;
                ms.Total = ms.MaxFund - ms.CommitmentsTotal;
                return Ok( new ResponoseMEssage() { Message="تم النسجيل بنجاح" , status= true , data = ms });
            }
            catch (Exception e)
            {

                // return InternalServerError(e);
                return Content(HttpStatusCode.NotFound, e);
            }




        }
        public IHttpActionResult ChangeStatus(MobileADO.NewLoanID model)
        {
            try
            {
                ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
                 ex = v.SessionValid(model.SessionID.ToString());
                if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }

                var c = db.MobileSessions.Where(x => x.SessionID.ToString() == model.SessionID).FirstOrDefault().UsersInfo.Customer;
                var l = c.LoansRequests.Where(x=> x.LoanID == model.CurrentLoanID).FirstOrDefault();

                if (l.StatusID == 0) 
                {

                    //  l.Audits.Add(new Audit { AuditDate = DateTimeOffset.UtcNow.DateTime.ToLocalTime(), AuditName = "العميل قام بتاكيد البينات", UserID = l.CustomerID });
                    _app.Audits.NewForLoanConfrimed(c.UsersInfo, l);
                     _app.LoanReq.LoanEquation(l);
                    db.SaveChanges();
                    return Ok( new ResponoseMEssage { Message = "تم ارسال الطلب الي الموظفين المسؤلين وسوف يتم اخطارك بالمستجدات قريبا", status=true });
                }
                else {ex = v.NewEx("هذا الطلب جاري العمل عليه ولايمكن التعديل في بياناته", _app.Validation.ErrorType.Loan); return Ok(ex) ;  }

            }
            catch (Exception e)
            {

                return Ok(e);
            }




        }
        public IHttpActionResult LoanInfo(MobileSession model)
        {

            try
            {
                ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
                ex = v.SessionValid(model.SessionID.ToString());
                if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }


                var ms = db.MobileSessions.Find(model.SessionID);
                var lr = ms.UsersInfo.Customer.LoansRequests.OrderByDescending(x=> x.RequestDate).ToList();

               
                MobileLoanReuest ml = new MobileLoanReuest();
               

                var c = ms.UsersInfo.Customer;
                if (c == null) { ex = v.NewEx("هذا ليس بحساب مستخدم برجاء اعاده تسجيل الدخول", _app.Validation.ErrorType.Account); return Ok(ex); }


                ml.Customer = new MobileADO.Customer
                {


                    Name_f = c.UsersInfo.Name_f,
                    Name_l = c.UsersInfo.Name_l,
                    Name_m = c.UsersInfo.Name_m,
                    CityID = c.CityID,
                    CityLat = c.CityLat,
                    CityLng = c.CityLng,
                    //  CustomerID = c.CustomerID
                    JobID = c.JobID,
                    NationalID = c.NationalID,
                    FullName = c.UsersInfo.FullName,
                    PhoneNumber = c.UsersInfo.PhoneNumber,
                    Photo = _app.ui.photo.AbsoluteUrl(c.UsersInfo.Photo)

                };
                
               
               



                List< MonileLoanrequestDetails> rdl = new List<MonileLoanrequestDetails> ();

                MonileLoanrequestDetails r = new MonileLoanrequestDetails();
                foreach (var l in lr)
                {

                    r = new MonileLoanrequestDetails();
                    r.BankName = l.Bank.BankName;
                    r.LoanID = l.LoanID;
                    if (l.Branch != null) { r.BranchName = l.Branch.BranchName; r.BranchID = l.BranchID.Value; r.BranchLat = l.Branch.lat; r.BranchLng = l.Branch.lng; }

                    if (l.Commitments.Count > 0) { r.Commitments = new List<MobileADO.Commitment>(); }
                    foreach (var com in l.Commitments.ToList())
                    {

                        r.Commitments.Add(new  MobileADO.Commitment { CommitmentName = com.CommitmentName, CommitmentValue = com.CommitmentValue });
                    }

                   
                   
                  //  r.Funds = l.Funds;
                    r.RequestDate = l.RequestDate;
                    r.Salary = l.Salary;
                    r.StatusName = l.LoanStatus.StatusName;
                    r.CustomerBranchID = l.CustomerSelectedBranchID.ToString();
                    rdl.Add(r);
                }

              
                


            

             
                ml.LoanList = rdl;

                //  ml.ResponoseMEssage = new ResponoseMEssage { status=true };
                ResponoseMEssage rm = new ResponoseMEssage();
                rm.status = true;
                rm.data = ml;
               



                return Ok(rm);
            }
            catch (Exception e)
            {

                return Ok(e);
            }




        }
        public IHttpActionResult UpdateProfile(MobileCustomerInfo model)
        {

            try
            {

                ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
                ex = v.SessionValid(model.SessionID.ToString());
                if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }

               

                var u = db.MobileSessions.Where(x => x.SessionID.ToString()
                == model.SessionID).FirstOrDefault().UsersInfo.Customer;
                ex = v.CustomerValied(model.Customer);
                if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }
               
                u.UsersInfo.Name_f = model.Customer.Name_f;
                u.UsersInfo.Name_m = model.Customer.Name_m;
                u.UsersInfo.Name_l = model.Customer.Name_l;
                u.UsersInfo.Password = model.Customer.Password;
                u.UsersInfo.Photo = _app.ui.photo.replacePhoto(u.UsersInfo.Photo, model.Customer.Photo);
                u.JobID = model.Customer.JobID;
                u.CityID = model.Customer.CityID;
                u.NationalID = model.Customer.NationalID;

                db.SaveChanges();

                return Ok( new ResponoseMEssage { Message = "تم تحديث البيانات", status=true });

            }
            catch (Exception e)
            {

                return InternalServerError(e);
            }



        }
        public IHttpActionResult Profile(MobileSession model)
        {

            try
            {
                ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
                ex = v.SessionValid(model.SessionID.ToString());
                if (ex.errors.Count >0) { return Content(HttpStatusCode.OK, ex); }


                ResponoseMEssage r = new ResponoseMEssage();

                var q = from x in db.Customers where x.UsersInfo.MobileSessions.FirstOrDefault(y=> y.SessionID == model.SessionID).SessionID == model.SessionID
                        select new { x.CityID , x.JobID, x.NationalID ,x.UsersInfo.Name_f, x.UsersInfo.Name_m
                , x.UsersInfo.Name_l, Photo = _app.ui.photo.AbsoluteUrl( x.UsersInfo.Photo) , x.UsersInfo.Password  } ;

                if (q.FirstOrDefault() == null) { ex = v.NewEx("لايوجد بيانات", _app.Validation.ErrorType.Account); return Ok(ex); }


                //List<object> rt = new List<object>();
                //rt.Add(q.FirstOrDefault());
                //rt.Add(new ResponoseMEssage { status= true });

                r.status = true;
                r.data = q;

                return Ok(r);

            }
            catch (Exception e)
            {

                return InternalServerError(e);
            }



        }



        public IHttpActionResult UpdateOrAddLoan(string value)


        {

            //MobileADO.mobileTestLoan model = Newtonsoft.Json.JsonConvert.DeserializeObject<MobileADO.mobileTestLoan>(value);
            //MobileLoanReuest mr = new MobileLoanReuest();
            //mr.Customer = new MobileADO.Customer();
            //mr.LoansRequest = new MonileLoanrequestDetails();

            //mr.Customer.CityID = _app.model.NullToInt(model.CustomerCityID);
            //mr.Customer.CityLat = model.CustomerCityLat;
            //mr.Customer.CityLng = model.CustomerCityLng;
            //mr.Customer.JobID = _app.model.NullToInt(model.CustomerJobID);
            //mr.Customer.Name_f = model.CustomerName_f;
            //mr.Customer.Name_l = model.CustomerName_l;
            //mr.Customer.Name_m = model.CustomerName_m;
            //mr.Customer.NationalID = model.CustomerNationalID;
            //mr.SessionID = model.SessionID;
            //mr.LoansRequest.BankID = _app.model.NullToInt(model.LoansRequestBankID);
            //mr.LoansRequest.Funds = _app.model.NullToInt(model.LoansRequestFunds);
            //mr.LoansRequest.LoanID = _app.model.NullToInt(model.LoansRequestLoanID);
            //mr.LoansRequest.Salary = _app.model.NullToInt(model.LoansRequestSalary);
            //mr.LoansRequest.Commitments = model.Commitment;
            //return UpdateOrAddNewLoan(mr);

            // MobileLoanReuest mobile = new MobileLoanReuest() {  };

            MobileLoanReuest model = Newtonsoft.Json.JsonConvert.DeserializeObject<MobileLoanReuest>(value);



            return UpdateOrAddNewLoan(model);


        }







    }
}