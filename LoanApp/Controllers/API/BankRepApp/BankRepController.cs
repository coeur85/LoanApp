﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Controllers.API.BankRepApp
{
    public class BankRepController : ApiController
    {

        LoanAppDBEntities db = new LoanAppDBEntities();
        _app.Validation v = new _app.Validation();
        
        public IHttpActionResult AddBankInfo( MobileADO.AddLoanBankInfo model)
        {


            try
            {

                ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
                ex = v.SessionValid(model.SessionID);
                if (ex.errors.Count > 0) { return Ok(ex); }

                var u = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID).UsersInfo;
                ex = v.BankRepValid(u.BankRepresentative);
                if (ex.errors.Count > 0) { return Ok(ex); }

                ex = v.AddLoanBankInfo(model);
                if (ex.errors.Count > 0) { return Ok(ex); }


                

               
                var m = db.MobileSessions.Where(x => x.SessionID.ToString() == model.SessionID).FirstOrDefault();

                var br = m.UsersInfo.BankRepresentative;

                var l = db.LoansRequests.Where(x => x.LoanID ==model.LoanID).FirstOrDefault();

                if (l.BankRepresentative != br) { ex = v.NewEx("هذا الطلب ليس محول لهذا المستخدم", _app.Validation.ErrorType.Loan); return Ok(ex); }

                l.MaxFunds = model.MaxFund;
                l.EarlyPayment =model.EarlyPayment;
                l.Salary3Month =model.Salary3Month;
                l.BankNotes = model.BankNotes;
               // l.StatusID = 6;

                _app.Audits.NewForStatus6(br.UsersInfo, l);

                db.SaveChanges();
                ResponoseMEssage rs = new ResponoseMEssage();
                rs.Message = "تم الحفظ بنجاح";

                return Ok(rs);
                


            }
            catch (Exception e)
            {

                return InternalServerError(e);
            }



        }

    }
}
