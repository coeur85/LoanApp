﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LoanApp.Models.DataBaseModel;


namespace LoanApp.Controllers.API
{
    public class GetController : ApiController
    {

        LoanAppDBEntities db = new LoanAppDBEntities();
        _app.Validation v = new _app.Validation();
        public IHttpActionResult Banks()
        {
            var bl = db.Banks.ToList();
            db.Banks.RemoveRange(bl);

            foreach (var b in bl)
            {
                b.BankRepresentatives = null;
                b.LoansRequests = null;
                b.FundLoansRequests = null;
            }

            return Ok(bl);
        }
        public IHttpActionResult Jobs()
        {
            var jl = db.Jobs.ToList();
            db.Jobs.RemoveRange(jl);
            return Ok(jl);
        }
        public IHttpActionResult Cities()
        {
            var cl = db.Cities.ToList();
            db.Cities.RemoveRange(cl);
            return Ok(cl);
        }
        public IHttpActionResult Branches()
        {
            var br = db.Branchs.ToList();


            IEnumerable<MobileADO.Branches> mb = new List<MobileADO.Branches>();

            mb = from b in br select new MobileADO.Branches { BranchCode = b.BranchCode, BranchID = b.BranchID, BranchName = b.BranchName };

           
            return Ok(mb.ToList());
        }
        public IHttpActionResult WellcomeScreen()
        {
            MobileADO.MobileWelcomeScreen ws = new MobileADO.MobileWelcomeScreen();

            var pl = db.SystemSetups.Where(x => x.Code == (int)_app.SystemSetup.MobileAppPhotos).ToList();

            foreach (var p in pl)
            {
                ws.Photo.Add( new MobileADO.Photo { photo = _app.ui.photo.AbsoluteUrl(p.Value) });
            }

            ResponoseMEssage r = new ResponoseMEssage();
            r.data = ws;
            return Ok(r);

        }



        public IHttpActionResult MyLoans(MobileSession model)
        {



            try
            {

                var u = db.MobileSessions.Where(x => x.SessionID == model.SessionID).FirstOrDefault().UsersInfo;
                List<LoansRequest> ls = new List<LoansRequest>();
                List<MonileLoanrequestDetails> mobileList = new List<MonileLoanrequestDetails>();


                if (u.BankRepresentative != null) { ls = db.LoansRequests.Where(x => x.BankRepID == u.UserID && x.StatusID == 5).ToList(); }
                else if (u.SalesRepresentative != null)
                {
                    ls = db.LoansRequests.Where(x => x.SalesRepID == u.UserID).ToList();

                    if (model.status == 1) { ls = ls.Where(x => x.StatusID >= 1 && x.StatusID <= 7).ToList(); } // open loans only
                    else if (model.status == 2) { ls = ls.Where(x => x.StatusID >= 8).ToList(); } // closed loans only

                }

                ls = ls.OrderByDescending(x => x.RequestDate).ToList();



                MonileLoanrequestDetails mld = new MonileLoanrequestDetails();
                foreach (var l in ls)
                {
                    mld = new MonileLoanrequestDetails();

                    



                    mld.ActualFunds = l.ActualFunds;
                    mld.CustomerBankAccountNumber = l.BankAccount;

                    if (l.Bank != null) { mld.BankName = l.Bank.BankName; mld.BankID = l.BankID; }


                    mld.FullName = l.Customer.UsersInfo.FullName;
                    mld.NationalID = l.Customer.NationalID;
                    if (l.Branch != null)
                    {

                        mld.BranchName = _app.model.NullToBranch(l.Branch);
                        mld.BranchLat = l.Branch.lat;
                        mld.BranchLng = l.Branch.lng;
                      

                    }




                    mld.EarlyPayment = l.EarlyPayment;

                    mld.FundBankName = _app.model.NullToBank(l.FundBank);
                    //   mld.Funds = l.Funds;
                    mld.LoanID = l.LoanID;
                    mld.MaxFunds = l.MaxFunds;
                    mld.RequestDate = l.RequestDate;
                    mld.Salary = l.Salary;
                    mld.Salary3Month = l.Salary3Month;
                    mld.StatusName = l.LoanStatus.StatusName;

                    mld.LoanID = l.LoanID;
                    mld.FullName = l.Customer.UsersInfo.FullName;
                    if (l.Customer.City != null)
                    {
                        mld.City = l.Customer.City.CityName;
                        mld.CityLat = l.Customer.CityLat;
                        mld.CityLng = l.Customer.CityLng;
                        mld.PhoneNumber = l.Customer.UsersInfo.PhoneNumber;
                    }
                    foreach (var com in l.Commitments)
                    {
                        mld.Commitments.Add(new MobileADO.Commitment { CommitmentName = com.CommitmentName, CommitmentValue = com.CommitmentValue });
                    }
                    

                    mobileList.Add(mld);
                }





                ResponoseMEssage r = new ResponoseMEssage();
                r.data = mobileList;

                return Ok(r);
            }
            catch (Exception e)
            {

                return Ok(e);
            }


        }

        public IHttpActionResult UpdateMyPassword(MobileADO.UpdatePassword model)
        {
            var ex = v.SessionValid(model.SessionID);
            if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }

            ex = v.PasswordValid(model.Password);
            if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }
            var u = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID).UsersInfo;
            u.Password = model.Password;
            db.SaveChanges();

            ResponoseMEssage r = new ResponoseMEssage() { Message = "تم تغير كلمه المرور بنجاح" };
            return Ok(r);

        }
        public IHttpActionResult Notifications(MobileSession model)
        {

           // MobileADO.Notification not = new MobileADO.Notification();

            var ex = v.SessionValid(model.SessionID.ToString());
            if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }

            var u = db.MobileSessions.FirstOrDefault(x => x.SessionID == model.SessionID).UsersInfo;
            List<Notification> no = new List<Notification>();
            if (u.Customer != null) { no = db.Notifications.Where(x => x.ToCustomers == true).ToList(); }
            else if (u.SalesRepresentative != null) { no = db.Notifications.Where(x => x.ToSalesReps == true).ToList(); }
           else if (u.BankRepresentative != null) { no = db.Notifications.Where(x => x.ToBankReps == true).ToList(); }

            no = no.OrderByDescending(x => x.SentDate).ToList();
            List<MobileADO.Notification> not = new List<MobileADO.Notification>();

            foreach (var n in no)
            {
                not.Add(new MobileADO.Notification { Header = n.Header, NoID = n.NoID, Photo= _app.ui.photo.AbsoluteUrl( n.Photo),
                    SentDate = n.SentDate, Continet = n.Continet });
            }

            ResponoseMEssage r = new ResponoseMEssage();
            r.data = not;


            return Ok(r);

        }
        public IHttpActionResult NotificationDetails(MobileADO.NotficationDetails model)
        {

            // MobileADO.Notification not = new MobileADO.Notification();

            var ex = v.SessionValid(model.SessionID.ToString());
            if (ex.errors.Count > 0) { return Content(HttpStatusCode.OK, ex); }

            var u = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID).UsersInfo;

            var n = db.Notifications.FirstOrDefault(x => x.NoID == model.NotID);

            if (n == null) { ex = v.NewEx("رقم تنبيه غير سليم", _app.Validation.ErrorType.Account);return Ok(ex); }

            var valid = false;
            if ( u.Customer != null && n.ToCustomers == true ) { valid = true; }
           else if (u.SalesRepresentative != null && n.ToSalesReps == true) { valid = true; }
          else  if (u.BankRepresentative != null && n.ToBankReps == true) { valid = true; }


            if (valid) {

                MobileADO.Notification not = new MobileADO.Notification { Header = n.Header, NoID = n.NoID,
                    Photo = _app.ui.photo.AbsoluteUrl( n.Photo), SentDate = n.SentDate, Continet =  n.Continet };


                ResponoseMEssage r = new ResponoseMEssage();
                r.data = not;


                return Ok(r);
            }
            else {
                // ResponoseErrorMEssage r = new ResponoseErrorMEssage();
                ex = v.NewEx("غير مصرح لك برؤيه هذا التنبيه", _app.Validation.ErrorType.Account);
                return Ok(ex);

            }
        }
        public IHttpActionResult SendMail(MobileADO.eMailMessages model)
        {

            var ex = v.SessionValid(model.SessionID);
            if (ex.errors.Count > 0) { return Ok(ex); }

            ex = v.eMailMessage(model);
            if (ex.errors.Count > 0) { return Ok(ex); }


            LoanApp.Models.Classes.Send.eMail e = new Models.Classes.Send.eMail();

            var body = e.ContactUSMailTemplate();
            var u = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID).UsersInfo;
            body = body.Replace("{userid}", u.UserID.ToString());
            body = body.Replace("{fullname}", model.Name );
            body = body.Replace("{phonenumber}", model.PhoneNumber);
            body = body.Replace("{body}", model.Body);
            body = body.Replace("{NationalID}", model.NationalID);
           
            if (e.SendMail(model.Name, body))
            { ResponoseMEssage r = new ResponoseMEssage { status = true, Message = "تم ارسال رسالتك بنجاح وسوف يعاود فريق العمل الاتصال بك في اقرب فرصه" }; return Ok(r); }
            else { ex = v.NewEx("حدث خطاء ولم يتم ارسال الرساله", _app.Validation.ErrorType.Account); return Ok(ex); }




        }
        public IHttpActionResult ContactUs() {
            var u = db.SystemSetups.Where(x => x.Code == (int)_app.SystemSetup.ContactUs).ToList();


            List<MobileADO.NameValue> nv = new List<MobileADO.NameValue>();

            foreach (var item in u)
            {
                nv.Add(new MobileADO.NameValue { Name = item.Name , Value = item.Value });
            }
            ResponoseMEssage r = new ResponoseMEssage();
            r.data = nv;
            return Ok(r);
        }

    }
}