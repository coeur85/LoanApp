﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LoanApp.Models.DataBaseModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class LoanAppDBEntities : DbContext
    {
        public LoanAppDBEntities()
            : base("name=LoanAppDBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Audit> Audits { get; set; }
        public virtual DbSet<BankRepresentative> BankRepresentatives { get; set; }
        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Commitment> Commitments { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<LoanStatus> LoanStatuses { get; set; }
        public virtual DbSet<MenuHeader> MenuHeaders { get; set; }
        public virtual DbSet<MenuItem> MenuItems { get; set; }
        public virtual DbSet<MobileSession> MobileSessions { get; set; }
        public virtual DbSet<Receipt> Receipts { get; set; }
        public virtual DbSet<SalesRepresentative> SalesRepresentatives { get; set; }
        public virtual DbSet<UsersInfo> UsersInfoes { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<SystemSetup> SystemSetups { get; set; }
        public virtual DbSet<LoansRequest> LoansRequests { get; set; }
        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<Branch> Branchs { get; set; }
    }
}
