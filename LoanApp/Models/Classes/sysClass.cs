﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using LoanApp.Models.DataBaseModel;
using System.Web.Mvc;
using System.Reflection;
using System.Drawing;
using System.Data.Entity;

 namespace _app
{




    public   class ui {
   
    public class msg {
        private static string txt = null;

        public static string Show { get {
                var r = txt;
                txt = null;
                return r; }
            set { txt = value; } }

    }

    public class photo
    {
        private static string ConvertToBase64(Stream stream)
        {
            Byte[] inArray = new Byte[(int)stream.Length];

            stream.Read(inArray, 0, (int)stream.Length);
            //  Convert.ToBase64CharArray(inArray, 0, inArray.Length, outArray, 0);
            return Convert.ToBase64String(inArray);
            // return new MemoryStream(Encoding.UTF8.GetBytes(outArray));
        }
            private static string fileNameAndEtention(string base64Photo)
            {
                string base64 = base64Photo.Substring(base64Photo.IndexOf(',') + 1);
                string[] sar = base64Photo.Split(';');
                var ext = sar[0].Remove(0, sar[0].IndexOf("/") + 1);

                if (ext == "jpeg") { ext = "jpg"; }

                string fileName = sar[1].Remove(10, sar[1].Length - 50);
                fileName = fileName.Replace("/", "1");
                fileName = fileName.Replace("=", "2");
                fileName = fileName.Replace(".", "3");
                fileName = fileName.Replace(",", "4");
                fileName = fileName.Replace("+", "5");

                return (fileName + "." + ext);
            }
        
        public static string replacePhoto(string oldfile, HttpFileCollectionBase file)

        {

            if (file.Count > 0)
            {

                var fe = file[0];

                if (fe.ContentType != "application/octet-stream")
                {
                    return "data:" + fe.ContentType + ";base64," + ConvertToBase64(fe.InputStream);



                }




            }

            return oldfile;


        }
        public static string replacePhoto(string oldfile, string file)

            {

                if (!string.IsNullOrEmpty(file)) { return file; }
                else if (!string.IsNullOrEmpty(oldfile)) { return oldfile; }
                else { UsersInfo u = new UsersInfo(true); return u.Photo; }
                

            }

            public static string AbsoluteUrl(string base64Photo)
            {
                string url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery,  string.Empty);
                string base64 = base64Photo.Substring(base64Photo.IndexOf(',') + 1);
                byte[] bytes = Convert.FromBase64String(base64);


                var fileName = fileNameAndEtention(base64Photo);

                var relativePath = "~/ImageStorage/" + fileName ;
                var absolutePath = HttpContext.Current.Server.MapPath(relativePath);

                // check if this file was already created


                if (!System.IO.File.Exists(absolutePath))
                { 


                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    Image image = Image.FromStream(ms);
                    image.Save(absolutePath);
                }

                    }

                return url + "/" + relativePath.Replace("~/", "");
            }



            public static string LocalUrl(string base64Photo)
            {

                string base64 = base64Photo.Substring(base64Photo.IndexOf(',') + 1);
                byte[] bytes = Convert.FromBase64String(base64);

                var fileName = fileNameAndEtention(base64Photo);

                var relativePath = "~/ImageStorage/" + fileName;
                var absolutePath = HttpContext.Current.Server.MapPath(relativePath);

                // check if this file was already created


                if (!System.IO.File.Exists(absolutePath))
                {


                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        Image image = Image.FromStream(ms);
                        image.Save(absolutePath);
                    }

                }

                return "/" + relativePath.Replace("~/", "");
            }



        }

    public class current {



           

            public static UsersInfo User
            {
                get {
                    LoanAppDBEntities db = new LoanAppDBEntities();
                    return db.Employees.Find(9).UsersInfo;
                    //UsersInfo u = (UsersInfo)HttpContext.Current.Session["UserID"];
                    //if (u == null)
                    //{
                    //    u = new UsersInfo(true);// HttpContext.Current.Response.Redirect("Login/index");
                    //}
                    //return u;
                }

           
                set { HttpContext.Current.Session["UserID"] = value; } }

            public static Loans UserLoans()
            {
                var u = User;
                Loans l = new Loans();
                
                if (u.UserID != 0)
                {
                    LoanAppDBEntities db = new LoanAppDBEntities();
                    if (u.Employee.BranchID == 1)
                    {
                        l.NewLoans = db.LoansRequests.Where(x => x.StatusID == 1).OrderByDescending(x => x.RequestDate).Take(3).ToList();
                        l.DeclinedLoans = db.LoansRequests.OrderByDescending(x => x.RequestDate).ToList().Where(x => _app.LoanReq.DeclinedStatus().Any(y=> x.StatusID == y) ).Take(3).ToList();
                        l.CompleltedLoansData = db.LoansRequests.Where(x => x.StatusID == 7).OrderByDescending(x => x.RequestDate).Take(3).ToList();

                        l.NewLoansCount = db.LoansRequests.Where(x => x.StatusID == 1).Count();
                        l.DeclinedLoansCount = db.LoansRequests.ToList().Where(x => _app.LoanReq.DeclinedStatus().Contains(x.StatusID)).Count();
                        l.CompleltedLoansDataCount = db.LoansRequests.Where(x => x.StatusID == 7).Count();
                    }
                    else
                    {
                        l.BranchLoans = db.LoansRequests.Where(x => x.BranchID == u.Employee.BranchID && x.StatusID == 4).OrderByDescending(x => x.RequestDate).Take(3).ToList();
                        l.BranchLoansCount = db.LoansRequests.Where(x => x.BranchID == u.Employee.BranchID && x.StatusID == 4).Count();
                    }

                    return l;
                }
                else { return null; }


            }


            public static List<MenuHeader> AllMenuHeaders()
            {
                LoanAppDBEntities db = new LoanAppDBEntities();
                return db.MenuHeaders.Where(x => x.OnlyActionControl == false).ToList();

            }




            public static List<MenuHeader> UserSideMenu()
            {



                List<MenuHeader> Menu = (List<MenuHeader>)HttpContext.Current.Session["UserMenu"];

                if (Menu != null) { return Menu; }

                Menu = User.MenuHeaders.ToList();
                LoanAppDBEntities db = new LoanAppDBEntities();

                if (User.Employee.BranchID != 1)
                {
                    if (!Menu.Any(x => x.HeaderID == 5))
                    {


                        var m = db.MenuHeaders.FirstOrDefault(x => x.HeaderID == 5);
                        var bank = new MenuHeader { HeaderIcon = m.HeaderIcon, HeaderID = m.HeaderID, HeaderName = m.HeaderName };
                        bank.MenuItems.Add(m.MenuItems.FirstOrDefault(x => x.ItemID == 15));


                        Menu.Add(bank);

                    }

                    if (!Menu.Any(X => X.HeaderID == 4))
                    {

                        var m = db.MenuHeaders.FirstOrDefault(x => x.HeaderID == 4);
                        var contract = new MenuHeader { HeaderIcon = m.HeaderIcon, HeaderID = m.HeaderID, HeaderName = m.HeaderName };
                        contract.MenuItems.Add(m.MenuItems.FirstOrDefault(x => x.ItemID == 9));
                        contract.MenuItems.Add(m.MenuItems.FirstOrDefault(x => x.ItemID == 10));
                        contract.MenuItems.Add(m.MenuItems.FirstOrDefault(x => x.ItemID == 11));


                        Menu.Add(contract);

                    }

                }
                Menu = Menu.OrderBy(x => x.HeaderID).ToList();
               HttpContext.Current.Session["UserMenu"] = Menu;
                return Menu;



        }

            public class Loans
            {
                public List<LoansRequest> NewLoans { get; set; }
                public int NewLoansCount { get; set; }
                public List<LoansRequest> DeclinedLoans { get; set; }
                public int DeclinedLoansCount { get; set; }
                public List<LoansRequest> CompleltedLoansData { get; set; }
                public int CompleltedLoansDataCount { get; set; }
                public List<LoansRequest> BranchLoans { get; set; }
                public int BranchLoansCount { get; set; }



                public Loans() { NewLoans = new List<LoansRequest>(); DeclinedLoans = new List<LoansRequest>();
                    CompleltedLoansData = new List<LoansRequest>(); BranchLoans = new List<LoansRequest>(); }
            }

        }

        public static DateTime DateNow
        {
            get
            {
                return TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Arab Standard Time"));

            }
        }


    }

    public class model {
        private LoanAppDBEntities db = new LoanAppDBEntities();
        public static bool userCanBeDelete(LoanApp.Models.DataBaseModel.UsersInfo u)
        {

            if(u.Audits != null) { if (u.Audits.Count > 0) { return false; } }
           

            if (u.Customer != null)
            {
                if (u.Customer.LoansRequests.Count > 0) { return false; }
                
            }

            if (u.BankRepresentative != null)
            {
                if (u.BankRepresentative.LoansRequests.Count > 0) { return false; }
               
            }

            if (u.SalesRepresentative != null)
            {
                if (u.SalesRepresentative.LoansRequests.Count > 0) { return false; }


            }


            return true;
        }

        public static bool bankCanBeDelete(LoanApp.Models.DataBaseModel.Bank b)
        {
            if (b.LoansRequests.Count > 0) { return false; }
            if (b.BankRepresentatives.Count > 0) { return false; }

            return true;
        }
        public static bool CityCanBeDelete(LoanApp.Models.DataBaseModel.City c)
        {
            if (c.Customers.Count > 0) { return false; }
           

            return true;
        }

        public void restPassword(int uid, string password)
        {
            var u = db.UsersInfoes.Find(uid);
            u.Password = password;
            db.SaveChanges();
        }


        public static bool branchCanBeDelete(LoanApp.Models.DataBaseModel.Branch br)
        {
            if (br.Employees.Count > 0) { return false; }
            if (br.LoansRequests.Count > 0) { return false; }

            return true;

        }


       public static string NullToString(object Value)
        {

            // Value.ToString() allows for Value being DBNull, but will also convert int, double, etc.
            return Value == null ? "" : Value.ToString();

            // If this is not what you want then this form may suit you better, handles 'Null' and DBNull otherwise tries a straight cast
            // which will throw if Value isn't actually a string object.
            //return Value == null || Value == DBNull.Value ? "" : (string)Value;


        }
        public static Int32 NullToInt(object Value)
        {

            // Value.ToString() allows for Value being DBNull, but will also convert int, double, etc.
            return Value == null ? 0 : Convert.ToInt32(Value) ;

            // If this is not what you want then this form may suit you better, handles 'Null' and DBNull otherwise tries a straight cast
            // which will throw if Value isn't actually a string object.
            //return Value == null || Value == DBNull.Value ? "" : (string)Value;


        }



        public static string NullToBank(Bank b)
        {

            if (b != null) { return NullToString(b.BankName); }
            else return null;
        }
        public static string NullToBranch(Branch b)
        {
            if (b != null) { return NullToString(b.BranchName); }
            else return null;

        }

        public class TimeAgo {
            public DateTime Date { get; set; }
            public string Class { get; set; }
        }

    }


    public class LoanReq
    {

        public static void LoanEquation(LoansRequest l)
        {

            Bank b = new Bank();

            if (l.Bank == null)
            {
                LoanAppDBEntities db = new LoanAppDBEntities();
                b = db.Banks.FirstOrDefault(x => x.BankID == l.BankID);
            }
            else { b = l.Bank; }

            var salInt = l.Salary * b.IntersetsRate;

            if (salInt >= l.CommitmentsTotal) { _app.Audits.NewForStatus1(null, l); }
            else { _app.Audits.NewForStatus2(null, l); }


        }

        public static List<int> ClosedStatus() { var closed = new[] { 2, 8, 9, 10 }; return closed.ToList(); }
        public static List<int> CanBeEditedStatus() { var edit = new[] { 0, 1, 2, 3, 10 }; return edit.ToList(); }
        public static List<int> DeclinedStatus() { var Declined = new[] { 2, 10 }; return Declined.ToList(); }
        public static List<int> CanBeTransferdToBranchEmp () { var transfer = new[] { 4,5,6,7 }; return transfer.ToList(); }
    }

    public class Notifecations
    {
        public static string LoanPage() { return "loans"; }
        public static string NotifecationsPage() { return "notifications"; }

    }

    public class Audits
    {

        private static Audit NewAudit(UsersInfo u, LoansRequest l)
        {
            Audit a = new Audit();
           // a.UsersInfo = u;
            a.AuditDate = _app.ui.DateNow;
            if (u != null) { a.UserID = u.UserID; }
            l.Audits.Add(a);
            return a;

        }

        public static void NewForStatus0(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u,l); a.NewStatusID = 0; a.AuditName = "تم اضافه طلب عقد جديد "; l.StatusID = 0; }


        public static void NewForStatus1(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.NewStatusID = 1; a.AuditName = "تم قبول الطلب بواسطه التظام"; l.StatusID = 1; }

        public static void NewForStatus2(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.NewStatusID = 2; a.AuditName = "تم رفض الطلب بواسطه النظام لعدم تحقيقه الشروط"; l.StatusID = 2; }

        public static void NewForStatus4(UsersInfo u, LoansRequest l, Branch branch)
        { var a = NewAudit(u, l); a.NewStatusID = 4; a.AuditName = "تم التحويل الي فرع " + branch.BranchName; l.StatusID = 4;


            LoanAppDBEntities db = new LoanAppDBEntities();
            var br = db.Branchs.Find(branch.BranchID);
            var q = br.Employees.Select(x => new
            {
                EmpID = x.EmpID,
                Count = x.LoansRequests.Where(y =>
                _app.LoanReq.CanBeTransferdToBranchEmp().Any(z=> z == y.StatusID) ).Count()
            }).ToList();

            if (q.Count > 0) {
                l.BranchEmpID = q.OrderByDescending(x => x.Count).FirstOrDefault().EmpID;
                NewForEmptransfer(null,l) ;
            }




        }

        public static void NewForStatus5(UsersInfo u, LoansRequest l, BankRepresentative br)
        { var a = NewAudit(u, l); a.NewStatusID = 5;
            a.AuditName = "تم التحويل الي بنك " + br.Bank.BankName + " الي المندوب البنكي " + br.UsersInfo.FullName
                ; l.StatusID = 5; }

        public static void NewForStatus6(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u,l); a.NewStatusID = 6; a.AuditName = "تم استيفاء البيانات البنكيه "; l.StatusID = 6; }

        public static void NewForStatus7(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.NewStatusID = 7; a.AuditName = "تم استيفاء بيانات القرض من  قبل موظف الفرع"; l.StatusID = 7; }

        public static void NewForStatus8(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.NewStatusID = 8; a.AuditName = "تم رفض الطلب من قبل مدير التظام"; l.StatusID = 8; }

        public static void NewForStatus9(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.NewStatusID = 9; a.AuditName = "تم قبول الطلب من قبل مدير النظام"; l.StatusID = 9; }

        public static void NewForStatus10(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.NewStatusID = 10; a.AuditName = "تم رفض الطلب من قبل موظف الفرع"; l.StatusID = 10; }

        public static void NewForCustomerEdit(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l) ; a.AuditName = "تم تعديل بيانات الطلب من قبل العميل"; }

        public static void NewForLoanConfrimed(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.AuditName = "تم التاكيد علي بيانات القرض"; }

        public static void NewForSalesrEdit(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.AuditName = "تم تعديل بيانات الطلب من قبل المندوب"; }

        public static void NewForEmpEdit(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.AuditName = "تم تعديل بيانات الطلب من موظف البرنامج"; }

        public static void NewForSalesrUpdateOldLoans(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.AuditName = "تم تحديث حاله الطلب عندنا استكمل المنودب باقي بيانات العميل"; a.NewStatusID = l.StatusID; }

        public static void NewForEmptransfer(UsersInfo u , LoansRequest l) {
            var a = NewAudit(u, l);
            a.AuditName = " تحويل الطلب الي الموظف  ";
        }

        //-------------  audit from commitments  -----------------

        public static void NewForCommitmentAddNew(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.AuditName = "تم اضافه التزام جديد"; }
        public static void NewForCommitmentDelete(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.AuditName = "تم حذف التزام "; }
        public static void NewForCommitmentUpdate(UsersInfo u, LoansRequest l)
        { var a = NewAudit(u, l); a.AuditName = "تم التعديل في بيانات التزام"; }

    }


    enum SystemSetup
    {
        MobileAppPhotos = 1,
        ContactUs = 2
    }



   

}





[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
public class MultipleButtonAttribute : ActionNameSelectorAttribute
{
    public string Name { get; set; }
    public string Argument { get; set; }

    public override bool IsValidName(ControllerContext controllerContext, string actionName, MethodInfo methodInfo)
    {
        var isValidName = false;
        var keyValue = string.Format("{0}:{1}", Name, Argument);
        var value = controllerContext.Controller.ValueProvider.GetValue(keyValue);

        if (value != null)
        {
            controllerContext.Controller.ControllerContext.RouteData.Values[Name] = Argument;
            isValidName = true;
        }

        return isValidName;
    }
}






        namespace LoanApp.Models.DataBaseModel
        {
            using System;
            using System.Data.Entity;
            using System.Data.Entity.Infrastructure;
    
            public partial class LoanAppDBEntities : DbContext
        {

            public override int SaveChanges()
        {
            var modified = this.ChangeTracker.Entries().Where(e => e.State == System.Data.Entity.EntityState.Modified);


            try
            {
                    return base.SaveChanges();
            }
            catch (Exception e)
            {

                if (e.InnerException.InnerException.Message.Contains("IX_PhoneNumber")) { _app.ui.msg.Show = "رقم هاتف هذا مسجل من قبل لا يمكن الحفظ"; }




                throw;
            }


           
            }
            }
        }