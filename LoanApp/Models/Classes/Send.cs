﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using LoanApp.Models.DataBaseModel;
using System.Net.Mail;

namespace LoanApp.Models.Classes
{
    public class Send
    {
        public class Notifications
        {

            string applicationID = "AAAA6JVWals:APA91bEhN-pugRuTQyo990y1T6Hpk8KrNAGzZ00QpVJjgiXJI15WfDsRjUcEDxUuosSqyyeDHG6a5wSJ5vjDc_QySFUwuHyDv8BSr6RHDWBwFrKCJcTJagEltQwm_x00YzXXKSAGnXFR";
            string senderId = "998937881179";


            //public string PushNotification(string token) //string Message, string Title, string token
            //    {
            //        try
            //        {


            //            string deviceId = token;

            //            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

            //            tRequest.Method = "post";

            //            tRequest.ContentType = "application/json";

            //            var data = new

            //            {

            //                to = deviceId,

            //                notification = new

            //                {

            //                    body = "Message",

            //                    title = "Title",

            //                    sound = "Default",

            //                    icon = "ic_app", //"myicon"

            //                    click_action = ""// default page open in android name and you can escap this...

            //            },
            //                priority = "high"
            //            };

            //            var serializer = new JavaScriptSerializer();

            //            var json = serializer.Serialize(data);

            //            Byte[] byteArray = Encoding.UTF8.GetBytes(json);

            //            tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

            //            tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

            //            tRequest.ContentLength = byteArray.Length;

            //            tRequest.UseDefaultCredentials = true;
            //            tRequest.PreAuthenticate = true;
            //            tRequest.Credentials = CredentialCache.DefaultCredentials;

            //            using (Stream dataStream = tRequest.GetRequestStream())
            //            {

            //                dataStream.Write(byteArray, 0, byteArray.Length);


            //                using (WebResponse tResponse = tRequest.GetResponse()) //Error here (The remote server returned an error: (401) Unauthorized.)
            //                {

            //                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
            //                    {

            //                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
            //                        {

            //                            String sResponseFromServer = tReader.ReadToEnd();

            //                            string str = sResponseFromServer;

            //                            //alert(str);
            //                            return str;

            //                        }
            //                    }
            //                }
            //            }
            //        }

            //        catch (Exception ex)
            //        {
            //            string str = ex.Message;
            //            //alert(str);
            //            return str;
            //        }

            //    }

            public string SendNotification(MobileSession ms, Notification noti) //string Message, string Title, string token
            {
                try
                {


                    string deviceId = ms.MobileID;

                    WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                    tRequest.Method = "post";

                    tRequest.ContentType = "application/json";


                    //var notification = new

                    //{

                    //    to = deviceId,
                    //    message_id = "m-1366082849205",


                    //    data = new { body = noti.Continet,

                    //       title = noti.Header, data = 

                    //    new { page = noti.PageName, loanID = noti.LoanID.ToString() }

                    //    }


                    //    ,
                    //    priority = "high"
                    //};

                    //var notification = new

                    //{

                    //    to = deviceId,
                    //    message_id = "m-1366082849205",
                    //    data = new

                    //    {

                    //        body = noti.Continet,

                    //        title = noti.Header,

                    //        sound = "Default",

                    //        icon = "ic_app", //"myicon"

                    //        click_action = noti.PageName, // default page open in android name and you can escap this...
                    //        tag = noti.LoanID

                    //    },
                    //    priority = "high"
                    //};


                    var data = new
                    {
                        notification = new
                        {
                            title = noti.Header ,  //Any value
                            body = noti.Continet ,  //Any value
                            sound = "default", //If you want notification sound
                         //  click_action = noti.PageName ,///"FCM_PLUGIN_ACTIVITY",  //Must be present for Android
                            icon = "fcm_push_icon",  //White icon Android resource
                         //   tag = noti.LoanID
                        },
                        data = new
                        {
                            page = noti.PageName,  //Any data to be retrieved in the notification callback
                            loanID = noti.LoanID
                        },
                        to =  deviceId , //Topic or single device
                        priority = "high", //If not set, notification won't be delivered on completely closed iOS app
                        restricted_package_name = "" //Optional. Set for application filtering
                    };










                    var serializer = new JavaScriptSerializer();

                    var json = serializer.Serialize(data);

                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                    tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                    tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                    tRequest.ContentLength = byteArray.Length;

                    tRequest.UseDefaultCredentials = true;
                    tRequest.PreAuthenticate = true;
                    tRequest.Credentials = CredentialCache.DefaultCredentials;

                    using (Stream dataStream = tRequest.GetRequestStream())
                    {

                        dataStream.Write(byteArray, 0, byteArray.Length);


                        using (WebResponse tResponse = tRequest.GetResponse()) //Error here (The remote server returned an error: (401) Unauthorized.)
                        {

                            using (Stream dataStreamResponse = tResponse.GetResponseStream())
                            {

                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {

                                    String sResponseFromServer = tReader.ReadToEnd();

                                    string str = sResponseFromServer;

                                    //alert(str);
                                    return str;

                                }
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    string str = ex.Message;
                    //alert(str);
                    return str;
                }

            }


            //private string SendNotification(string token, string tilte) //string Message, string Title, string token
            //    {
            //        try
            //        {


            //            string deviceId = token;

            //            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

            //            tRequest.Method = "post";

            //            tRequest.ContentType = "application/json";

            //            var data = new

            //            {

            //                to = deviceId,

            //                notification = new

            //                {

            //                  //  body = message,

            //                    title = tilte,

            //                    sound = "Default",

            //                    icon = "ic_app", //"myicon"

            //                    click_action = ""//default page open in android name and you can escap this...


            //                },
            //                priority = "high"
            //            };

            //            var serializer = new JavaScriptSerializer();

            //            var json = serializer.Serialize(data);

            //            Byte[] byteArray = Encoding.UTF8.GetBytes(json);

            //            tRequest.Headers.Add(string.Format("Authorization: key={0} ", applicationID));

            //            tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

            //            tRequest.ContentLength = byteArray.Length;

            //            tRequest.UseDefaultCredentials = true;
            //            tRequest.PreAuthenticate = true;
            //            tRequest.Credentials = CredentialCache.DefaultCredentials;

            //            using (Stream dataStream = tRequest.GetRequestStream())
            //            {

            //                dataStream.Write(byteArray, 0, byteArray.Length);


            //                using (WebResponse tResponse = tRequest.GetResponse()) //Error here (The remote server returned an error: (401) Unauthorized.)
            //                {

            //                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
            //                    {

            //                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
            //                        {

            //                            String sResponseFromServer = tReader.ReadToEnd();

            //                            string str = sResponseFromServer;

            //                            //alert(str);
            //                            return str;

            //                        }
            //                    }
            //                }
            //            }
            //        }

            //        catch (Exception ex)
            //        {
            //            string str = ex.Message;
            //            //alert(str);
            //            return str;
            //        }

            //    }



            public string SendNotificationForLoan(UsersInfo u, string title, string content, LoansRequest loan)
            {



                Notification noti = new Notification();
                noti.Header = title;
                noti.Continet = content;
                noti.SentDate = _app.ui.DateNow;
                noti.ToBankReps = false;
                noti.ToCustomers = false;
                noti.ToSalesReps = false;
                noti.ToUser = u.UserID;
                noti.LoanID = loan.LoanID;
                noti.PageName = _app.Notifecations.LoanPage();
                noti.CreatedEmp = _app.ui.current.User.UserID;

                LoanAppDBEntities db = new LoanAppDBEntities();
                db.Notifications.Add(noti);
                db.SaveChanges();

                var ms = u.MobileSessions.OrderByDescending(x => x.ExpireDate).FirstOrDefault();

                if (ms != null)
                {


                    return SendNotification(ms, noti);
                }
                else { return "false"; }
            }



            public string SendNotification(UsersInfo u, Notification noti)
            {
                var ms = u.MobileSessions.OrderByDescending(x => x.ExpireDate).FirstOrDefault();

                if (ms != null) { return SendNotification(ms, noti); }
                else { return null; }

            }


        }
        public class TextMessage



        {
            private string MobilyUserName = "966555035615";
            private string MobilyPassword = "95175326548Ealan";



            private string FormatePhoneNUmber(string phoneNumer)
            {
                if (phoneNumer.StartsWith("05")) { phoneNumer = phoneNumer.Remove(0, 1); phoneNumer = ("0966" + phoneNumer); }

                return phoneNumer;
            }



            public int SendConfirmText(string phoneNumer, string code)
            {

                phoneNumer = FormatePhoneNUmber(phoneNumer);
                string text = "كود التفعيل الخاص بك : " + code;
                return Convert.ToInt32(SendMessage(text, phoneNumer));
                // return sendText(phoneNumer, code);

            }

            public int SendRestPasswordTest(string phoneNumer, string password)
            {

                phoneNumer = FormatePhoneNUmber(phoneNumer);
                string text = "كلمه المرور الجديده هي : " + password;
                return Convert.ToInt32(SendMessage(text, phoneNumer));
                // return sendText(phoneNumer, code);

            }




            public string GetBalance()
            {
                //int temp = '0';
                //WebResponse myResponse = null;

                try
                {
                    HttpWebRequest req = (HttpWebRequest)
                    WebRequest.Create("http://www.mobily.ws/api/balance.php");
                    req.Method = "POST";
                    req.ContentType = "application/x-www-form-urlencoded";
                    string postData = "mobile=" + MobilyUserName + "&password=" + MobilyPassword;
                    req.ContentLength = postData.Length;

                    StreamWriter stOut = new
                    StreamWriter(req.GetRequestStream(),
                    System.Text.Encoding.ASCII);
                    stOut.Write(postData);
                    stOut.Close();
                    // Do the request to get the response
                    string strResponse;
                    StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
                    strResponse = stIn.ReadToEnd();
                    stIn.Close();

                    if (strResponse == "1" || strResponse == "2" || strResponse == "-2" || strResponse == "-1")
                    { return "error"; }

                    return strResponse;
                }
                catch (Exception)
                {

                    return "error";
                }


            }


            private string SendMessage(string msg, string numbers)
            {
                //int temp = '0';

                HttpWebRequest req = (HttpWebRequest)
                WebRequest.Create("http://www.mobily.ws/api/msgSend.php");
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                string postData = "mobile=" + MobilyUserName + "&password=" + MobilyPassword + "&numbers=" + numbers + "&sender=" + "EalanNajd" +
                    "&msg=" + ConvertToUnicode(msg) + "&applicationType=59";
                req.ContentLength = postData.Length;

                StreamWriter stOut = new
                StreamWriter(req.GetRequestStream(),
                System.Text.Encoding.ASCII);
                stOut.Write(postData);
                stOut.Close();
                // Do the request to get the response
                string strResponse;
                StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
                strResponse = stIn.ReadToEnd();
                stIn.Close();
                return strResponse;
            }

            private string ConvertToUnicode(string val)
            {
                string msg2 = string.Empty;

                for (int i = 0; i < val.Length; i++)
                {
                    msg2 += convertToUnicode(System.Convert.ToChar(val.Substring(i, 1)));
                }

                return msg2;
            }
            private string convertToUnicode(char ch)
            {
                System.Text.UnicodeEncoding class1 = new System.Text.UnicodeEncoding();
                byte[] msg = class1.GetBytes(System.Convert.ToString(ch));

                return fourDigits(msg[1] + msg[0].ToString("X"));
            }

            private string fourDigits(string val)
            {
                string result = string.Empty;

                switch (val.Length)
                {
                    case 1: result = "000" + val; break;
                    case 2: result = "00" + val; break;
                    case 3: result = "0" + val; break;
                    case 4: result = val; break;
                }

                return result;
            }
        }
        public class eMail
        {


            public bool SendMail(string subject , string body )
            {


                try
                {
                SmtpClient client = new SmtpClient("webmail.ealan-najd-co.com");
                //If you need to authenticate
                client.Credentials = new NetworkCredential("Mobile.App@ealan-najd-co.com", "P@$$w0rd");
               

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("Mobile.App@ealan-najd-co.com","تطبيق الجوال") ;
                mailMessage.To.Add("Mobile.App@ealan-najd-co.com");
                mailMessage.Subject = subject;
                mailMessage.Body = body ;
                mailMessage.IsBodyHtml = true;
              

                client.Send(mailMessage);
                return true;
                }
                catch (Exception e)
                {

                    throw e;
                }
              

            }


            public string ContactUSMailTemplate()
            {


                //string text;
                //var fileStream = new FileStream(HttpContext.Current.Server.MapPath(@"html\contactusMailTemplate.html"), FileMode.Open, FileAccess.Read);
                //using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                //{
                //    text = streamReader.ReadToEnd();
                //}

                //return text;

                string[] readText = File.ReadAllLines( HttpContext.Current.Server.MapPath("~/html/contactusMailTemplate.html"));
                StringBuilder strbuild = new StringBuilder();
                foreach (string s in readText)
                {
                    strbuild.Append(s);
                    strbuild.AppendLine();
                }
                return  strbuild.ToString();

            }

        }

    }
}