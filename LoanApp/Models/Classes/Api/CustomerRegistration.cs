﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LoanApp.Models.DataBaseModel;

namespace LoanApp.Models.Classes.Api
{
    public class CustomerRegistration
    {
        LoanAppDBEntities db = new LoanAppDBEntities();
        Send.TextMessage st = new Send.TextMessage();

        [System.Web.Http.HttpPost]
        public int RegesterNewCustomer(LoginModel mo)
        {
            Customer c = new Customer();
            c.UsersInfo.PhoneNumber = mo.PhoneNumber;
            c.UsersInfo.Password = mo.Password;
            var r  = new Random();
            c.ActivationCode = r.Next(1000, 10000).ToString();
            c.ActivationCodeDate = _app.ui.DateNow;
          
            db.Customers.Add(c);

            var i = db.SaveChanges();
            st.SendConfirmText(mo.PhoneNumber, c.ActivationCode);

            return c.CustomerID;


        }

    }
}


namespace _app
{

    public class mobileApp {
        public static MobileADO.MobileSession LogUser (UsersInfo u , MobileLoginModel model)
        {
            LoanAppDBEntities db = new LoanAppDBEntities();


            try
            {
                MobileSession ms = new MobileSession();

                ms = db.MobileSessions.Where(x => x.UsersInfo.PhoneNumber == model.PhoneNumber
                && x.ExpireDate > _app.ui.DateNow).FirstOrDefault();


                if (ms != null)
                {
                    if (ms.MobileID != model.MobileID)
                    {
                        ms.ExpireDate = _app.ui.DateNow.AddDays(-1);
                        db.SaveChanges();
                        ms = null;
                    }

                }



                 if (ms == null)
                {
                    ms = new MobileSession();
                    ms.SessionID = Guid.NewGuid();
                    ms.UserID = u.UserID;
                    ms.MobileID = model.MobileID;
                    ms.ExpireDate = _app.ui.DateNow.AddDays(2);
                    db.MobileSessions.Add(ms);
                    db.SaveChanges();
                }
               

                MobileADO.MobileSession mm = new MobileADO.MobileSession();
                mm.SessionID = ms.SessionID;
                mm.FullName = u.FullName;
                mm.Name_f = u.Name_f;
                mm.Name_m = u.Name_m;
                mm.Name_l = u.Name_l;
                mm.Photo = _app.ui.photo.AbsoluteUrl( u.Photo);
                mm.AccountType = u.AccountType;

                //if (u.Customer != null) { mm.AccountType = "Customer"; }
                //else if (u.SalesRepresentative != null) { mm.AccountType = "SalesRepresentative"; }
                //else if (u.BankRepresentative != null) { mm.AccountType = "BankRepresentative"; }
                //   db.Configuration.LazyLoadingEnabled = true;
              //  db.MobileSessions.Remove(ms);
                return mm;
            }
            catch (Exception )
            {

                throw;
            }

            


        }

    }
}