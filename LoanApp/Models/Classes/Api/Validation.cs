﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using LoanApp.Models.DataBaseModel;

namespace _app
{
    public class Validation
    {

          ResponoseErrorMEssage ex = new ResponoseErrorMEssage();
        //public ResponoseErrorMEssage ex {
        //    get { if (e. == null) { e.error = new ResponoseErrorMEssage.errors() { Messages = new List<string>() }; } return e;
        //       // if (e.error.Messages == null) {  }
        //    }
        //    set { value = e; } }

        // ResponoseErrorMEssage ex = new ResponoseErrorMEssage();


       public enum ErrorType
        {
            Phone = 0,
            Password = 1,
            Account = 2,
            Loan = 3,
            Name = 4
        }


        
        public ResponoseErrorMEssage NewEx( string msg, ErrorType errorType)
        { // ex.status = false;

         //   if (ex.errors == null) { ex.errors = new Errors(); }

            switch (errorType)
            {
                case ErrorType.Phone:
                    if (ex.errors.phone == null) { ex.errors.phone = new List<Phone>(); }
                   ex.errors.phone.Add(new Phone { message = msg });
                    break;
                case ErrorType.Password:
                    if (ex.errors.password == null) { ex.errors.password = new List<Password>(); }
                    ex.errors.password.Add(new Password { message = msg });
                    break;
                case ErrorType.Account:
                    if (ex.errors.account == null) { ex.errors.account = new List<Account>(); }
                    ex.errors.account.Add(new Account { message = msg });
                    break;
                case ErrorType.Loan:
                    if (ex.errors.loan == null) { ex.errors.loan = new List<Loan>(); }
                    ex.errors.loan.Add(new Loan { message = msg });
                    break;
                case ErrorType.Name:
                    if (ex.errors.name == null) { ex.errors.name = new List<Name>(); }
                    ex.errors.name.Add( new Name { message = msg } );
                    break;
                default:
                    break;
            }

            

            return ex;
        }
        LoanAppDBEntities db = new LoanAppDBEntities();



        private ResponoseErrorMEssage NotRegestred()
        {
          //  var u = db.UsersInfoes.Where(x => x.PhoneNumber == ph).FirstOrDefault();
         //  if (u == null) { return (NewEx("Phone number is not regestred")); }
            return NewEx("رقم هاتف غير مسجل لدينا", ErrorType.Account);
        }
        private UsersInfo user(string ph) { return db.UsersInfoes.FirstOrDefault(x => x.PhoneNumber == ph); }
        private void ActiveAccount(UsersInfo u)
        {
          

            if (u.ActiveAccount == true) { NewEx("هذا الحساب نشط بالفعل", ErrorType.Account); }

        }
        private void NotActiveAccount(UsersInfo u)
        {


            if (u.ActiveAccount == false) { NewEx("هذا الحساب غير نشط", ErrorType.Account); }
        }




        public ResponoseErrorMEssage SessionValid(string sessionID)
        {


            var m = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == sessionID);


            return SessionValid(m);
        }
        public ResponoseErrorMEssage SessionValid(MobileSession ms)
        {

            if (ms == null) { NewEx("برجاء اعاده تسجيل الدخول", ErrorType.Account); return ex; }
            if (ms.ExpireDate < _app.ui.DateNow) { NewEx("برجاء اعاده تسجيل الدخول", ErrorType.Account); }
            NotActiveAccount(ms.UsersInfo);

            


            return ex;
        }

        public ResponoseErrorMEssage CustomerValied(MobileADO.Customer c)
        {
            if (c == null) { NewEx("خطاء في بيانات العميل", ErrorType.Account); return ex; }
            if (string.IsNullOrEmpty(c.Name_f)) { NewEx("برجاء توفير الاسم الاول", ErrorType.Name); }
            if (string.IsNullOrEmpty(c.Name_m)) { NewEx("برجاء توفير الاسم الثاني", ErrorType.Name); }
            if (string.IsNullOrEmpty(c.Name_l)) { NewEx("برجاء توفير الللقب", ErrorType.Name); }

            if (!c.CityID.HasValue || c.CityID == 0) { NewEx("برجاء اختيار المدينه", ErrorType.Account); }
            if (!c.JobID.HasValue || c.JobID == 0) { NewEx("برجاء اختيار الوظيفه", ErrorType.Account); }
            if (string.IsNullOrEmpty(c.NationalID)) { NewEx("برجاء ادخال رقم الهويه", ErrorType.Account); }
            if (c.NationalID.Any(x=>  char.IsLetter(x)) ) {NewEx("رقم الهويه لايجب ان يحتوي علي حروف او مسافات", ErrorType.Account); }



            return ex;
        }
        public ResponoseErrorMEssage LoanValied(MonileLoanrequestDetails  r)
        {
            if (r == null) { NewEx("خطاء في بيانات القرض", ErrorType.Loan); return ex; }
            if (r.BankID == 0) { NewEx("لابد من اختيار البتك", ErrorType.Loan); }
            if (! string.IsNullOrEmpty(r.CustomerBranchID))
            { var b = db.Branchs.FirstOrDefault(x=> x.BranchID.ToString() == r.CustomerBranchID) ;
                if (b == null) {NewEx("كود فرع غير سليم", ErrorType.Loan); }
 }
            // if (db.Branchs.FirstOrDefault(x => x.BranchID.ToString() == r.CustomerBranchID) == null) { NewEx("لابد من تحديد كود فرع سليم", ErrorType.Loan); }

            if (r.Commitments.Count == 0) { NewEx("علي الافل لابد من توفير التزام واحد", ErrorType.Loan); }
            else
            {
                var b = db.Banks.FirstOrDefault(x => x.BankID == r.BankID);
                if (b != null)
                {
                    if (b.Active == false) { NewEx("البنك لا يستقبل الطلبات", ErrorType.Loan); }
                    if (!r.Commitments.Any(x => x.CommitmentName == b.BankName))
                    { NewEx("الالتزام الاول لابد ان يكون بأسم البنك المطلوب منه  القرض", ErrorType.Loan); }
                }
                else { NewEx("بنك غير سليم", ErrorType.Loan); }
                
            }
        

            //if (r.Funds.ToString().Any(x => char.IsLetter(x))){ NewEx("التمويل المطلوب ليس صحيحا", ErrorType.Loan);  }
            //if (r.Funds <=0 ) { NewEx("لابد ان اكون قيمه التمويل اكثر من صفر", ErrorType.Loan); }


            if (r.Salary.ToString().Any(x => char.IsLetter(x))) { NewEx("برجاء توفير بيانات راتب سليمه", ErrorType.Loan); }
           // if (r.Funds <= 0) { NewEx("الراتب لابد ان يكون اكثر من صفر", ErrorType.Loan); }





            if (r.Commitments.Any(x => string.IsNullOrEmpty(x.CommitmentName))) { NewEx("لابد من اضافه اسماء كل الالتزامات", ErrorType.Loan); }
            if (r.Commitments.Any(x => string.IsNullOrEmpty(x.CommitmentValue.ToString()))) { NewEx("لابد من اضافه قيمه كل التزام", ErrorType.Loan); }






            return ex;
        }

        public ResponoseErrorMEssage BankRepValid(BankRepresentative b)
        {
            if (b == null) { NewEx("هذا ليس حساب موظف بتكي", ErrorType.Account); return ex; } ;
            if (b.UsersInfo.ActiveAccount != true) { NewEx("حسابك غير نشط", ErrorType.Account); }

            return ex;
        }
        public ResponoseErrorMEssage AddLoanBankInfo(MobileADO.AddLoanBankInfo m)
        {

            var l = db.LoansRequests.FirstOrDefault(x => x.LoanID == m.LoanID);
            if (l.StatusID != 5) { NewEx("هذا الطلب ليس في حاله استيفاء المعلومات البنكيه", ErrorType.Loan); return ex; }

            if (m.EarlyPayment.ToString().Any(x => char.IsLetter(x)) ||
                 m.Salary3Month.ToString().Any(x => char.IsLetter(x)) ||
                 m.MaxFund.ToString().Any(x => char.IsLetter(x)))
            {
                NewEx("لا يمكن قبول غير ارقام في في اي من هذه البيانات", ErrorType.Loan);
                return ex;
            }

            if (m.EarlyPayment <= 0 ||
                m.Salary3Month <= 0 ||
                m.MaxFund <= 0)
            {
                NewEx("لابد من تحديد ارقام  ولابد ان تكون اكبر من صفر", ErrorType.Loan);
             //   return ex;
                
            }


            

            return ex;
        }



        public ResponoseErrorMEssage PhoneNumer(string ph)
        {
            
            if (ph.Any(x => char.IsLetter(x))) {NewEx("هذا ليس برقم", ErrorType.Phone) ; }

            if (!ph.Contains("05")) {NewEx("الرقم المدرج ليس رقم جوال سعودي", ErrorType.Phone); }
            else { if (ph.IndexOf("05") != 0) { ph = ph.Remove(0,ph.IndexOf("05")); } }
            if (ph.Length != 10) { NewEx( "رقم الجوال لابد ان يكون 10 ارقام", ErrorType.Phone);  }
            



            return ex;



        }
        public ResponoseErrorMEssage SingUp(string ph) {

            PhoneNumer(ph);
            if (db.UsersInfoes.Any(x => x.PhoneNumber == ph)) { NewEx("هذا الرقم مسجل لدينا من قبل", ErrorType.Phone); }
            return ex;
        }
        public ResponoseErrorMEssage ConfirmCode(string ph)
        {
            PhoneNumer(ph);
            //  ex.Add(NotRegestred(ph));
            var u = user(ph);
             if (u == null) { NotRegestred(); }
           
            else if (u.ActiveAccount == true) { ActiveAccount(u) ; }

            return ex;
        }
        public ResponoseErrorMEssage UpdatePassword(MobileLoginModel m)
        {
           

            PhoneNumer(m.PhoneNumber);
            var u = user(m.PhoneNumber);
            if (u == null) { NotRegestred(); }
            else
            {
                if(u.ActiveAccount== false) { NotActiveAccount(u) ; return ex; }
                //  if (m.Password.Length < 6) { NewEx("كلمه المرور يجب ان نكون 6 ارقام او حروف علي الاقل", ErrorType.Password); }
                PasswordValid(m.Password);


               
            }
            return ex;
        }
        public ResponoseErrorMEssage PasswordValid(string password)
        {

            if (password.Length < 6) { NewEx("كلمه المرور يجب ان نكون 6 ارقام او حروف علي الاقل", ErrorType.Password); }
            return ex;
        }
        public ResponoseErrorMEssage RestPassword(string ph)
        {
            PhoneNumer(ph);
            var u = user(ph);
            if (u == null) { NotRegestred(); return ex; }
         ///   if (u.ActiveAccount == false) { NewEx("هذا الحساب ليس مفعل لابد من توفير الكود المرسل في الرساله النصيه", ErrorType.Account); return ex; }



            return ex;
        }

        public ResponoseErrorMEssage eMailMessage(MobileADO.eMailMessages model)
        {
            if (string.IsNullOrEmpty(model.Name)) { NewEx("برجاء ادخال الاسم", ErrorType.Account); }
            if (string.IsNullOrEmpty(model.Body)){ NewEx("برجاء ادخال محتوي الرساله", ErrorType.Account); }
            PhoneNumer(model.PhoneNumber);
            if (string.IsNullOrEmpty(model.NationalID)) { NewEx("برجاء ادخال رقم الهويه", ErrorType.Account); }

            return ex;

        }


    }

}

