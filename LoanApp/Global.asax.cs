﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using System.Web.Optimization;
using System.IO;
using System.Collections.Generic;

namespace LoanApp
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            BundelConfig.RegesterBundels(BundleTable.Bundles);

            // ADD MY SECURTY CLASS
            GlobalFilters.Filters.Add(new WebPagesAuthorize() );
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            // add api json Serialize


        }


        protected void Application_Error()
        {
            var ex = Server.GetLastError();

            if (ex.Message.Contains("Server cannot set status after HTTP headers have been sent")) { return; }

            var errorinfor = "user :" + _app.ui.current.User.UserID + " At date and time:" + _app.ui.DateNow.ToString();
            string newline = (errorinfor + Environment.NewLine + ex );


            string File1 = Server.MapPath("~/error.txt");
            FileStream t1 = new System.IO.FileStream(File1, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite);
            StreamReader readFile = new System.IO.StreamReader(t1);
            List<String> List1 = new List<String>();
            String getLine = "";

            while (readFile.Peek() >= 0)
            {
                getLine = readFile.ReadLine();
                List1.Add(getLine);
            }
            readFile.Close();
            t1.Close();

            /*....................*/
            newline = (newline + Environment.NewLine + "---------------" + Environment.NewLine + getLine);
            FileStream t2 = new System.IO.FileStream(File1, System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.ReadWrite);
            StreamWriter writeFile = new System.IO.StreamWriter(t2);
            writeFile.WriteLine(newline);
            writeFile.Close();
            t2.Close();



        }

    }
}