
delete from [dbo].[Audits]
delete from [dbo].[Commitments]
delete from [dbo].[Notifications]
delete from [dbo].[LoansRequests]
delete from [dbo].[BankRepresentatives]
delete from [dbo].[Branchs]
delete from [dbo].[Customers]
delete from [dbo].[Cities]

delete from [dbo].[Employees] where EmpID <> 9
delete from [dbo].[Jobs]
delete from [dbo].[MobileSessions]

delete from [dbo].[Receipts]
delete from [dbo].[SalesRepresentatives]
delete from [dbo].[UserHeaderMenu] where UserID <> 9
delete from [dbo].[UsersInfo] where UserID <> 9